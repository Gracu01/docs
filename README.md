# Gothic 2 Online - Docs
Website available on: https://gothicmultiplayerteam.gitlab.io/docs/  
Offline documentation: https://gitlab.com/api/v4/projects/26628284/jobs/artifacts/master/raw/g2o_docs.zip?job=offline

# Using mike to generate version documentation
- Create or update documentation
```commandline
mike deploy $VERSION
```

- Set as default
```commandline
mike set-default $VERSION
```

- Serve debug server
```commandline
mike serve
```
