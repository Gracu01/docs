---
title: 'getPlayerMaxHealth'
---
# `function` getPlayerMaxHealth <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player max health points.

## Declaration
```cpp
int getPlayerMaxHealth(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the maximum health points amount.

