---
title: 'stopAniId'
---
# `function` stopAniId <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"
!!! note
    Animation ids aren't constant, and depend on Humans.mds file.

This function is used to stop animation on player for all players.

## Declaration
```cpp
void stopAniId(int id, int aniId = -1)
```

## Parameters
* `int` **id**: the player id.
* `int` **aniId**: the animation id. The default value is `-1` which means that the first active ani will be stopped.
  

