---
title: 'getPlayerStrength'
---
# `function` getPlayerStrength <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player strength points.

## Declaration
```cpp
int getPlayerStrength(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the strength points amount.

