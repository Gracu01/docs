---
title: 'getPlayerPing'
---
# `function` getPlayerPing <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    The function can return `-1` if player isn't connected.

This function will get the player ping.
Ping gets updated after each 2500 miliseconds.

## Declaration
```cpp
int getPlayerPing(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the current player ping.

