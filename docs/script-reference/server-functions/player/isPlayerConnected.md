---
title: 'isPlayerConnected'
---
# `function` isPlayerConnected <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether player is connected to the server.

## Declaration
```cpp
bool isPlayerConnected(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool`
`true` when player is connected, otherwise `false`.

