---
title: 'getPlayerAmulet'
---
# `function` getPlayerAmulet <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the equipped player amulet.

## Declaration
```cpp
string|null getPlayerAmulet(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the item instance from Daedalus scripts.

