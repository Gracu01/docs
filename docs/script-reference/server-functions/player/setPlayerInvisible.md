---
title: 'setPlayerInvisible'
---
# `function` setPlayerInvisible <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.5"

This function will toggle the player invisiblity for all players.  
The invisible player will still see other visible players.

## Declaration
```cpp
void setPlayerInvisible(int id, bool toggle)
```

## Parameters
* `int` **id**: the player id.
* `bool` **toggle**: `true` if the player should be invisible for all players, otherwise `false`.
  

