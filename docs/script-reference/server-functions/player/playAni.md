---
title: 'playAni'
---
# `function` playAni <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"

This function is used to play animation on player for all players.

## Declaration
```cpp
void playAni(int id, string aniName)
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: the name of the animation, e.g: `"T_STAND_2_SIT"`.
  

