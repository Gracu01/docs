---
title: 'getPlayerBelt'
---
# `function` getPlayerBelt <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the equipped player belt.

## Declaration
```cpp
string|null getPlayerBelt(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the item instance from Daedalus scripts.

