---
title: 'ban'
---
# `function` ban <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    The reason string can't be longer than 255 characters.

This function will ban the player on the server.

## Declaration
```cpp
void ban(int id, int minutes, string reason)
```

## Parameters
* `int` **id**: the player id.
* `int` **minutes**: the time how long ban will take in minutes. Passing `0` will cause the player to have permanent ban.
* `string` **reason**: the reason why player was banned.
  

