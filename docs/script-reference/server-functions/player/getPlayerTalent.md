---
title: 'getPlayerTalent'
---
# `function` getPlayerTalent <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player talent.

## Declaration
```cpp
bool getPlayerTalent(int id, int talentId)
```

## Parameters
* `int` **id**: the player id.
* `int` **talentId**: the talent id. For more information see [Talent constants](../../../shared-constants/talent/).
  
## Returns `bool`
`true` if talent is enabled for player, otherwise `false`.

