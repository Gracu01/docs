---
title: 'unspawnPlayer'
---
# `function` unspawnPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Unspawned players can't see other players, items, etc. and are invisible for others.

This function will unspawn the player.

## Declaration
```cpp
void unspawnPlayer(int id)
```

## Parameters
* `int` **id**: the player id.
  

