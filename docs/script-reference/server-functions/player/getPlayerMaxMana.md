---
title: 'getPlayerMaxMana'
---
# `function` getPlayerMaxMana <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This function will get the player max mana points.

## Declaration
```cpp
int getPlayerMaxMana(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the maximum mana points amount.

