---
title: 'getPlayerMagicLevel'
---
# `function` getPlayerMagicLevel <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This function will get the player magic level.

## Declaration
```cpp
int getPlayerMagicLevel(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the player magicLevel.

