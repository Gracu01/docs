---
title: 'getPlayerContext'
---
# `function` getPlayerContext <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This function is used to get player script context.
For more information see [this article](../../../../multiplayer/script-context/).

## Declaration
```cpp
int getPlayerContext(int id, int type)
```

## Parameters
* `int` **id**: the player id.
* `int` **type**: the type of context.
  
## Returns `int`
the value stored within selected context.

