---
title: 'kick'
---
# `function` kick <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    The reason string can't be longer than 255 characters.

This function will kick the player from the server.

## Declaration
```cpp
void kick(int id, string reason)
```

## Parameters
* `int` **id**: the player id.
* `string` **reason**: the reason why player was kicked.
  

