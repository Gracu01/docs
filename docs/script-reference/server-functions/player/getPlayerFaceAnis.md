---
title: 'getPlayerFaceAnis'
---
# `function` getPlayerFaceAnis <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will get the currently played face animations on player.

## Declaration
```cpp
[{aniName, layer}] getPlayerFaceAnis(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `[{aniName, layer}]`
the array of objects describing face animation.

