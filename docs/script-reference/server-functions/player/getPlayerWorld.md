---
title: 'getPlayerWorld'
---
# `function` getPlayerWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.4"

This function will get the player world.

## Declaration
```cpp
string getPlayerWorld(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string`
the player world.

