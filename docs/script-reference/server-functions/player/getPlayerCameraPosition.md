---
title: 'getPlayerCameraPosition'
---
# `function` getPlayerCameraPosition <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.0"

This function will get the player camera position in world.

## Declaration
```cpp
Vec3 getPlayerCameraPosition(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `Vec3`
the vector that represents camera position.

