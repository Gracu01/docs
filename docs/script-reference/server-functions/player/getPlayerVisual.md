---
title: 'getPlayerVisual'
---
# `function` getPlayerVisual <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player visual.

## Declaration
```cpp
{bodyModel, bodyTxt, headModel, headTxt} getPlayerVisual(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `{bodyModel, bodyTxt, headModel, headTxt}`
player visual.

