---
title: 'setPlayerPosition'
---
# `function` setPlayerPosition <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the player world position for all players.

## Declaration
```cpp
void setPlayerPosition(int id, float x, float y, float z)
```

## Parameters
* `int` **id**: the player id.
* `float` **x**: the position in the world on the x axis.
* `float` **y**: the position in the world on the y axis.
* `float` **z**: the position in the world on the z axis.
  

