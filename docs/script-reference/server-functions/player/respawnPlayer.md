---
title: 'respawnPlayer'
---
# `function` respawnPlayer <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function will immediately respawn the player if is dead.

## Declaration
```cpp
void respawnPlayer(int id)
```

## Parameters
* `int` **id**: the player id.
  

