---
title: 'getPlayerDexterity'
---
# `function` getPlayerDexterity <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player dexterity points.

## Declaration
```cpp
int getPlayerDexterity(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the dexterity points amount.

