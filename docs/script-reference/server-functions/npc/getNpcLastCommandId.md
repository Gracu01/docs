---
title: 'getNpcLastCommandId'
---
# `function` getNpcLastCommandId <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This function returns last finished command id by given NPC.
Every command in queue has associated unique id, by which task can be identified.

## Declaration
```cpp
int getNpcLastCommandId(int id)
```

## Parameters
* `int` **id**: the npc id.
  
## Returns `int`
the last finished command id. If there is no finished command -1 is returned instead.

