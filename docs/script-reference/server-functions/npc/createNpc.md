---
title: 'createNpc'
---
# `function` createNpc <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"
!!! note
    By default npcs won't be added to world. In order to do that, you have to call [spawnPlayer](../../player/spawnPlayer/).
!!! note
    Remote NPC id will always begins from max slots value.

This function creates remote NPC.

## Declaration
```cpp
int createNpc(string name, string instance)
```

## Parameters
* `string` **name**: the displayed name of the npc.
* `string` **instance**: the instance name of for the npc.
  
## Returns `int`
The id of created npc. If id is set to `-1` then creation of npc failed.

