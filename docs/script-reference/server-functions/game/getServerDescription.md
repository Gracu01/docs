---
title: 'getServerDescription'
---
# `function` getServerDescription <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.0"

This function will get the description of the server.

## Declaration
```cpp
string getServerDescription()
```

## Parameters
No parameters.
  
## Returns `string`
Server description.

