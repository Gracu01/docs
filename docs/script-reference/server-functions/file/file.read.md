---
title: 'file.read'
---
# `function` file.read <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"
!!! note
    This method is a extension of the original [file class](http://www.squirrel-lang.org/squirreldoc/stdlib/stdiolib.html#the-file-class).

This method will read a text from a file.

## Declaration
```cpp
string file.read(int|string param = "l")
```

## Parameters
* `int|string` **param**: If param is integer, then the function will read x characters from a file. If param is a string, then the following commands are supported: - "a" - read whole file - "l" - read line without \n character - "L" - read line with \n character. If param is not passed, then by default line will be read from file.
  
## Returns `string`
the read text from a file.

