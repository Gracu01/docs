---
title: 'file.write'
---
# `function` file.write <font size="4">(server-side)</font>
!!! info "Available since version: 0.2"
!!! note
    This method is a extension of the original [file class](http://www.squirrel-lang.org/squirreldoc/stdlib/stdiolib.html#the-file-class).

This method will write a string into a file.

## Declaration
```cpp
void file.write(string text)
```

## Parameters
* `string` **text**: the text that you want to write to a file.
  

