---
title: 'getWorld'
---
# `function` getWorld <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

The function is used to get the path of the current world in game.

## Declaration
```cpp
string getWorld()
```

## Parameters
No parameters.
  
## Returns `string`
The world path name.

