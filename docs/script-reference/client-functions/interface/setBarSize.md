---
title: 'setBarSize'
---
# `function` setBarSize <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

This function will set the specified status bar size on the screen.

## Declaration
```cpp
void setBarSize(int type, int width, int height)
```

## Parameters
* `int` **type**: the type of status bar you want to set the size of. For more information see [HUD constants](../../../client-constants/hud/).
* `int` **width**: size on the screen in virtuals.
* `int` **height**: size on the screen in virtuals.
  

