---
title: 'getCursorSizePx'
---
# `function` getCursorSizePx <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the mouse cursor size on screen in pixels.

## Declaration
```cpp
{width, height} getCursorSizePx()
```

## Parameters
No parameters.
  
## Returns `{width, height}`
the cursor size on screen.

