---
title: 'getCursorPositionPx'
---
# `function` getCursorPositionPx <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This function will get the mouse cursor position on screen in pixels.

## Declaration
```cpp
{x, y} getCursorPositionPx()
```

## Parameters
No parameters.
  
## Returns `{x, y}`
the cursor position on screen.

