---
title: 'nay'
---
# `function` nay <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Use this function only when you want to convert y position or height on the screen.

This function will convert virtuals to pixels on screen Y dimension and return it as a result.

## Declaration
```cpp
int nay(int virtuals)
```

## Parameters
* `int` **virtuals**: the virtuals to convert.
  
## Returns `int`
the pixels after conversion.

