---
title: 'getCursorPosition'
---
# `function` getCursorPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the mouse cursor position on screen in virtuals.

## Declaration
```cpp
{x, y} getCursorPosition()
```

## Parameters
No parameters.
  
## Returns `{x, y}`
the cursor position on screen.

