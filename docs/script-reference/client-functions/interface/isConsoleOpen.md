---
title: 'isConsoleOpen'
---
# `function` isConsoleOpen <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"

The function is used to check whether the console is shown.

## Declaration
```cpp
bool isConsoleOpen()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when console is shown, otherwise `false`.

