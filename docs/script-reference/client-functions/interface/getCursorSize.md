---
title: 'getCursorSize'
---
# `function` getCursorSize <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the mouse cursor size on screen in virtuals.

## Declaration
```cpp
{width, height} getCursorSize()
```

## Parameters
No parameters.
  
## Returns `{width, height}`
the cursor size on screen.

