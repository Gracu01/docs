---
title: 'letterDistancePx'
---
# `function` letterDistancePx <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    This function should always return `1`, even for different fonts.

This function will get the hardcoded distance between letters in pixels.

## Declaration
```cpp
int letterDistancePx()
```

## Parameters
No parameters.
  
## Returns `int`
the distance between characters.

