---
title: 'getHudMode'
---
# `function` getHudMode <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

The function is used get the visibilty mode of HUD elements.

## Declaration
```cpp
int getHudMode(int type)
```

## Parameters
* `int` **type**: the type of HUD element you want to check visibility of. For more information see [HUD constants](../../../client-constants/hud/).
  
## Returns `int`
The visibility mode of specified HUD element.

