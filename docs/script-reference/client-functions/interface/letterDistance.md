---
title: 'letterDistance'
---
# `function` letterDistance <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! note
    This function should always return the same value for specific resolution, even for different fonts.

This function will get the hardcoded distance between letters in virtuals.

## Declaration
```cpp
int letterDistance()
```

## Parameters
No parameters.
  
## Returns `int`
the distance between characters.

