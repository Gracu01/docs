---
title: 'getBarSize'
---
# `function` getBarSize <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"

The function is used to retrieve specified status bar size on the screen.

## Declaration
```cpp
{width, height} getBarSize(int type)
```

## Parameters
* `int` **type**: the type of status bar you want to get the size of. For more information see [HUD constants](../../../client-constants/hud/).
  
## Returns `{width, height}`
Size of specified status bar on the screen.

