---
title: 'getMusicVolume'
---
# `function` getMusicVolume <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function will get the ingame music volume.

## Declaration
```cpp
float getMusicVolume()
```

## Parameters
No parameters.
  
## Returns `float`
volume value in range <0.0, 1.0>.

