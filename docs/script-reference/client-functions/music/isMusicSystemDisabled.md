---
title: 'isMusicSystemDisabled'
---
# `function` isMusicSystemDisabled <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function is used to check if ingame music system is disabled.

## Declaration
```cpp
bool isMusicSystemDisabled()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` if ingame music system is disabled, otherwise `false`.

