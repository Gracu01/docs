---
title: 'setMusicVolume'
---
# `function` setMusicVolume <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function will set the ingame music volume.

## Declaration
```cpp
void setMusicVolume(float volume)
```

## Parameters
* `float` **volume**: the value in range <0.0, 1.0>.
  

