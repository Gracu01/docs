---
title: 'getPingLimit'
---
# `function` getPingLimit <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"
!!! note
    By default the ping limit is set to 150.

This function will get the client ping limit.

## Declaration
```cpp
int getPingLimit()
```

## Parameters
No parameters.
  
## Returns `int`
the current ping limit.

