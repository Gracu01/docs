---
title: 'setExp'
---
# `function` setExp <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function will set hero current experience, which can be seen in statistics.

## Declaration
```cpp
void setExp(int exp)
```

## Parameters
* `int` **exp**: the hero experience.
  

