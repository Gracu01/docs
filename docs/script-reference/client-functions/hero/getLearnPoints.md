---
title: 'getLearnPoints'
---
# `function` getLearnPoints <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function is used to get current hero learn points.

## Declaration
```cpp
int getLearnPoints()
```

## Parameters
No parameters.
  
## Returns `int`
the current hero learn points.

