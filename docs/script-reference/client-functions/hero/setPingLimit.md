---
title: 'setPingLimit'
---
# `function` setPingLimit <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"
!!! note
    By default the ping limit is set to 150.
!!! note
    limit below `50` will be ignored.

This function will set the client ping limit.  
The ping limit controls when client cannot move due to big ping.

## Declaration
```cpp
void setPingLimit(int limit)
```

## Parameters
* `int` **limit**: the ping limit.
  

