---
title: 'getEq'
---
# `function` getEq <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

The function is used to get all of the items from hero inventory.

## Declaration
```cpp
[{instance, amount, name}...] getEq()
```

## Parameters
No parameters.
  
## Returns `[{instance, amount, name}...]`
the array populated with all of the item objects from hero inventory.

