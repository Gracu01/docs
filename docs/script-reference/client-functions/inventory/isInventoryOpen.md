---
title: 'isInventoryOpen'
---
# `function` isInventoryOpen <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

The function is used to check whether the game inventory UI is shown.

## Declaration
```cpp
bool isInventoryOpen()
```

## Parameters
No parameters.
  
## Returns `bool`
`true` when game inventory UI is shown, otherwise `false`.

