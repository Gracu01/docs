---
title: 'openInventory'
---
# `function` openInventory <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will show the game inventory UI.

## Declaration
```cpp
void openInventory()
```

## Parameters
No parameters.
  

