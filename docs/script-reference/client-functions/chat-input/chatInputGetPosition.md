---
title: 'chatInputGetPosition'
---
# `function` chatInputGetPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to retrieve the text input position on the screen.

## Declaration
```cpp
{x, y} chatInputGetPosition()
```

## Parameters
No parameters.
  
## Returns `{x, y}`
Position of text input on the screen.

