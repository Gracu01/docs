---
title: 'chatInputSend'
---
# `function` chatInputSend <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will send typed message to the server.  
In case when message is command (starts with forward slash `/`),  
server will trigger event onPlayerCommand, otherwise event onPlayerMessage will be called.  
It will also clear the chat input text and hide it from the screen.

## Declaration
```cpp
void chatInputSend()
```

## Parameters
No parameters.
  

