---
title: 'chatInputGetText'
---
# `function` chatInputGetText <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to retrieve the chat input text.

## Declaration
```cpp
string chatInputGetText()
```

## Parameters
No parameters.
  
## Returns `string`
text that is typed inside text input.

