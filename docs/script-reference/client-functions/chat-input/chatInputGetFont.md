---
title: 'chatInputGetFont'
---
# `function` chatInputGetFont <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

The function is used to retrieve the chat input font.

## Declaration
```cpp
string chatInputGetFont()
```

## Parameters
No parameters.
  
## Returns `string`
font that is used by chat input.

