---
title: 'chatInputSetText'
---
# `function` chatInputSetText <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the chat input text.

## Declaration
```cpp
void chatInputSetText(string text)
```

## Parameters
* `string` **text**: that will be set to chat input.
  

