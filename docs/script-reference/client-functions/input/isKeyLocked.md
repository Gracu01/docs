---
title: 'isKeyLocked'
---
# `function` isKeyLocked <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

The function is used to check whether the specified keyboard lock key is locked, e.g: CapsLock, NumLock, ScrollLock.

## Declaration
```cpp
bool isKeyLocked(int keyId)
```

## Parameters
* `int` **keyId**: the id of the key. For more information see [Key constants](../../../client-constants/key/).
  
## Returns `bool`
`true` when the key is locked, otherwise `false`.

