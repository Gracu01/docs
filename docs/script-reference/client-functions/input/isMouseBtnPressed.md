---
title: 'isMouseBtnPressed'
---
# `function` isMouseBtnPressed <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This function is used to check whether the specified mouse button is currently pressed.

## Declaration
```cpp
bool isMouseBtnPressed(int button)
```

## Parameters
* `int` **button**: . For more information see [Mouse constants](../../../client-constants/mouse/).
  
## Returns `bool`
`true` when specified mouse button is pressed, otherwise `false`.

