---
title: 'getLODStrengthModifier'
---
# `function` getLODStrengthModifier <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the LOD strength modifier used by dynamic models (ASC) like armors, npc body, etc...

## Declaration
```cpp
float getLODStrengthModifier()
```

## Parameters
No parameters.
  
## Returns `float`
The LOD strength value in range <0.0, 1.0>.

