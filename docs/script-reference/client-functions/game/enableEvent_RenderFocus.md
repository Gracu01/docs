---
title: 'enableEvent_RenderFocus'
---
# `function` enableEvent_RenderFocus <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.0"
!!! note
    By default this event is disabled.

This function will enable/disable execution of [onRenderFocus](../../../client-events/general/onRenderFocus/) event.

## Declaration
```cpp
void enableEvent_RenderFocus(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to enable triggering of this event, otherwise `false`.
  

