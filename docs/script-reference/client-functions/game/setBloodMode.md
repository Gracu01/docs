---
title: 'setBloodMode'
---
# `function` setBloodMode <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.6"
!!! note
    The specified blood mode must match one of the following: (`0` - NONE | `1` - PARTICLES | `2` - DECALS | `3` - TRAILS).
!!! note
    By default the blood mode option depends on blood detail option in the game settings.

This function will set the way of blood emission for all peds (players and npcs) when they are hurt.

## Declaration
```cpp
void setBloodMode(int mode)
```

## Parameters
* `int` **mode**: the mode of blood emission.
  

