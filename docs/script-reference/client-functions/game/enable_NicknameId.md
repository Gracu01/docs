---
title: 'enable_NicknameId'
---
# `function` enable_NicknameId <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.5"
!!! note
    By default players ids are showed.

This function will enable/disable showing of players ids in their nicknames.

## Declaration
```cpp
void enable_NicknameId(bool toggle)
```

## Parameters
* `bool` **toggle**: `true` when you want to show the id in the nickname, otherwise `false`.
  

