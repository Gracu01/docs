---
title: 'setLODStrengthModifier'
---
# `function` setLODStrengthModifier <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will set the LOD strength modifier used by dynamic models (ASC) like armors, npc body, etc...

## Declaration
```cpp
void setLODStrengthModifier(float strength)
```

## Parameters
* `float` **strength**: the LOD strength value in range <0.0, 1.0>.
  

