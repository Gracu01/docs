---
title: 'exitGame'
---
# `function` exitGame <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will close the game.

## Declaration
```cpp
void exitGame()
```

## Parameters
No parameters.
  

