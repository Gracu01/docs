---
title: 'playVideo'
---
# `function` playVideo <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9"

This function will play video in BIK format, available in the game video path (\_Work\Data\Video).

## Declaration
```cpp
bool playVideo(string name)
```

## Parameters
* `string` **name**: the path to the video file with extension e.g. "Logo1.bik".
  
## Returns `bool`
`true` if the video was found and is now playing, otherwise `false`.

