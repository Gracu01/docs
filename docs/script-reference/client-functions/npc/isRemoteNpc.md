---
title: 'isRemoteNpc'
---
# `function` isRemoteNpc <font size="4">(client-side)</font>
!!! info "Available since version: 0.3.0"

This function checks whether id related to given object is remote NPC.

## Declaration
```cpp
bool isRemoteNpc(int npcId)
```

## Parameters
* `int` **npcId**: the npc id.
  
## Returns `bool`
`true` when object is remote npc, otherwise `false`.

