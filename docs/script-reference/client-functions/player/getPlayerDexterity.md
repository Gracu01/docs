---
title: 'getPlayerDexterity'
---
# `function` getPlayerDexterity <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the player/npc dexterity points.

## Declaration
```cpp
int getPlayerDexterity(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the dexterity points amount.

