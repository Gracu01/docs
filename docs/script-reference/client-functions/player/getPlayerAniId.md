---
title: 'getPlayerAniId'
---
# `function` getPlayerAniId <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the active player/npc animation id.

## Declaration
```cpp
int getPlayerAniId(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
aniId the ani id.

