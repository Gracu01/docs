---
title: 'readySpell'
---
# `function` readySpell <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will cause hero/npc to ready equipped spell.

## Declaration
```cpp
void readySpell(int id, int slotId, int manaInvested)
```

## Parameters
* `int` **id**: the player id.
* `int` **slotId**: the equipped spell slotId in range <0, 6>.
* `int` **manaInvested**: the spell cast cost in mana points.
  

