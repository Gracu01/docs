---
title: 'setPlayerHealth'
---
# `function` setPlayerHealth <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will set the player/npc health points.

## Declaration
```cpp
void setPlayerHealth(int id, int health)
```

## Parameters
* `int` **id**: the npc id.
* `int` **health**: the health points amount.
  

