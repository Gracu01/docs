---
title: 'attackPlayerMagic'
---
# `function` attackPlayerMagic <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.3"
!!! note
    The victim will receive damage calculated damage by the game.

This function is used to force npc to magic attack the other npc.

## Declaration
```cpp
bool attackPlayerMagic(int attackerId, int victimId, string instance)
```

## Parameters
* `int` **attackerId**: the attacker id.
* `int` **victimId**: the victim id.
* `string` **instance**: the spell item instance from Daedalus scripts.
  
## Returns `bool`
`true` if attack was successfully executed, otherwise `false`.

