---
title: 'playGesticulation'
---
# `function` playGesticulation <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This function is used to play gesticulation animations on player/npc.

## Declaration
```cpp
void playGesticulation(int id)
```

## Parameters
* `int` **id**: the player id.
  

