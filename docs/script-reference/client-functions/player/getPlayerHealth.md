---
title: 'getPlayerHealth'
---
# `function` getPlayerHealth <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc health points.

## Declaration
```cpp
int getPlayerHealth(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the health points amount.

