---
title: 'removeEffect'
---
# `function` removeEffect <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.4.9"

This function is used to remove visual effect from player.

## Declaration
```cpp
bool removeEffect(int id, string effect)
```

## Parameters
* `int` **id**: the player id.
* `string` **effect**: the name of the VisualFX, e.g: `"SPELLFX_MAYA_GHOST"`.
  
## Returns `bool`
`true` if effect was successfully removed, otherwise `false`.

