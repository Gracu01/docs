---
title: 'getPlayerProtection'
---
# `function` getPlayerProtection <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the player protection.

## Declaration
```cpp
int getPlayerProtection(int id, int damageType)
```

## Parameters
* `int` **id**: the player id.
* `int` **damageType**: . For more information see [Damage constants](../../../shared-constants/damage/).
  
## Returns `int`
the protection value for specific damageType.

