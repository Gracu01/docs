---
title: 'setPlayerMaxHealth'
---
# `function` setPlayerMaxHealth <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.1"

This function will set the player/npc max health points.

## Declaration
```cpp
void setPlayerMaxHealth(int id, int maxHealth)
```

## Parameters
* `int` **id**: the player id.
* `int` **maxHealth**: the maximum health points amount.
  

