---
title: 'getPlayerColor'
---
# `function` getPlayerColor <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc nickname color for client.

## Declaration
```cpp
{r, g, b} getPlayerColor(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `{r, g, b}`
the player nickname color.

