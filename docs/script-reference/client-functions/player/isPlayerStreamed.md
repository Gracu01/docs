---
title: 'isPlayerStreamed'
---
# `function` isPlayerStreamed <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    This function should only be used on players.

The function is used to check if player is inside streamed area.  
Only players that are currently in streamed area, can be visible to your hero in game.  
Streamed area is basicly, a roller with some radius and height, and elements inside are controlled by server.

## Declaration
```cpp
bool isPlayerStreamed(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool`
`true` when player is streamed, otherwise `false`.

