---
title: 'getPlayerAniNameById'
---
# `function` getPlayerAniNameById <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the ani name by player/npc animation id.

## Declaration
```cpp
string getPlayerAniNameById(int id, int aniId)
```

## Parameters
* `int` **id**: the player id.
* `int` **aniId**: the animation id.
  
## Returns `string`
the ani name.

