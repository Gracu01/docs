---
title: 'isPlayerCreated'
---
# `function` isPlayerCreated <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

The function is used to check whether player is connected or npc is created.

## Declaration
```cpp
bool isPlayerCreated(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `bool`
toggle `true` when player is created, otherwise `false`.

