---
title: 'getPlayerSelectedSpellNr'
---
# `function` getPlayerSelectedSpellNr <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"
!!! note
    Equipped spells are numbered from 0 to 6.

This function will get currently selected spell number.

## Declaration
```cpp
int getPlayerSelectedSpellNr(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `int`
the number of selected spell from inventory.

