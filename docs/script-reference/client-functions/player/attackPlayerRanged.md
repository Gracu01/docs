---
title: 'attackPlayerRanged'
---
# `function` attackPlayerRanged <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.3"
!!! note
    The victim will receive damage calculated damage by the game.

This function is used to force npc to ranged attack the other npc.

## Declaration
```cpp
bool attackPlayerRanged(int attackerId, int victimId)
```

## Parameters
* `int` **attackerId**: the attacker id.
* `int` **victimId**: the victim id.
  
## Returns `bool`
`true` if attack was successfully executed, otherwise `false`.

