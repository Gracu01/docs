---
title: 'getPlayerAniIdByName'
---
# `function` getPlayerAniIdByName <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.10"

This function will get the ani id by player/npc animation name.

## Declaration
```cpp
int getPlayerAniIdByName(int id, string aniName)
```

## Parameters
* `int` **id**: the player id.
* `string` **aniName**: the animation name, e.g `"S_RUN"`.
  
## Returns `int`
the ani id.

