---
title: 'removeWeapon'
---
# `function` removeWeapon <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will cause hero/npc to hide a weapon.

## Declaration
```cpp
void removeWeapon(int id)
```

## Parameters
* `int` **id**: the player id.
  

