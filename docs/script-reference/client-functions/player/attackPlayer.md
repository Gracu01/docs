---
title: 'attackPlayer'
---
# `function` attackPlayer <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.3"
!!! note
    The victim will receive damage calculated damage by the game.

This function is used to force npc to melee attack the other npc.

## Declaration
```cpp
bool attackPlayer(int attackerId, int victimId, int type)
```

## Parameters
* `int` **attackerId**: the attacker id.
* `int` **victimId**: the victim id.
* `int` **type**: the id of the hit type. For more information see [Attack constants](../../../client-constants/attack/).
  
## Returns `bool`
`true` if attack was successfully executed, otherwise `false`.

