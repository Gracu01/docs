---
title: 'getPlayerPosition'
---
# `function` getPlayerPosition <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the player/npc world position.

## Declaration
```cpp
{x, y, z} getPlayerPosition(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `{x, y, z}`
the player world position.

