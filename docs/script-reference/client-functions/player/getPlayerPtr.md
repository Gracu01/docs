---
title: 'getPlayerPtr'
---
# `function` getPlayerPtr <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This function will get player npc pointer.

## Declaration
```cpp
userpointer getPlayerPtr(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `userpointer`
the raw pointer to npc object.

