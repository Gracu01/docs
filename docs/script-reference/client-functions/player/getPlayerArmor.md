---
title: 'getPlayerArmor'
---
# `function` getPlayerArmor <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the equipped player/npc armor.

## Declaration
```cpp
string|null getPlayerArmor(int id)
```

## Parameters
* `int` **id**: the player id.
  
## Returns `string|null`
the item instance from Daedalus scripts.

