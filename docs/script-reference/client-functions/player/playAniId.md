---
title: 'playAniId'
---
# `function` playAniId <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! note
    Animation ids aren't constant, and depend on Humans.mds file.

This function is used to play animation on player/npc.

## Declaration
```cpp
void playAniId(int id, int aniId)
```

## Parameters
* `int` **id**: the player id.
* `int` **aniId**: the animation id.
  

