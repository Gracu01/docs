---
title: 'onTime'
---
# `event` onTime <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered each time when game time minute passes.

## Parameters
```c++
int day, int hour, int min
```

* `int` **day**: the current ingame day.
* `int` **hour**: the current ingame hour.
* `int` **min**: the current ingame minutes.

