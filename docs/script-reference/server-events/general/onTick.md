---
title: 'onTick'
---
# `event` onTick <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.10"
!!! note
    This event is disabled by default. To enable it, use [enableEvent_onTick](../../../server-functions/game/enableEvent_onTick/) function.

This event is triggered in every server main loop iteration.

## Parameters
No parameters.

