---
title: 'onExit'
---
# `event` onExit <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.0"

This event is triggered when server is going to shut down.  
You can use it, to save some data before closing up, or do something else.

## Parameters
No parameters.

