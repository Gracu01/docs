---
title: 'onNPCCommandFinished'
---
# `event` onNPCCommandFinished <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when NPC finishes command.

## Parameters
```c++
int npc_id, int command_id
```

* `int` **npc_id**: the id of the remote npc.
* `int` **command_id**: the id of the finished command.

