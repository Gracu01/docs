---
title: 'onPlayerChangeWorld'
---
# `event` onPlayerChangeWorld <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player tries to change his currently played world (**ZEN**).

## Parameters
```c++
int playerid, string world
```

* `int` **playerid**: the id of the player who tries to change the played world.
* `string` **world**: an filename name of the world.

