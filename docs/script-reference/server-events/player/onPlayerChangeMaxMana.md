---
title: 'onPlayerChangeMaxMana'
---
# `event` onPlayerChangeMaxMana <font size="4">(server-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when player maximum mana changes.

## Parameters
```c++
int playerid, int previous, int current
```

* `int` **playerid**: the id of the player whose health points gets changed.
* `int` **previous**: the previous maximum mana points of the player.
* `int` **current**: the current maximum mana points of the player.

