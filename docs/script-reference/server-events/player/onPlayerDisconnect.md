---
title: 'onPlayerDisconnect'
---
# `event` onPlayerDisconnect <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when a player gets disconnected with the server.

## Parameters
```c++
int playerid, int reason
```

* `int` **playerid**: the id of the player who joined the server.
* `int` **reason**: the reason why player got disconnected. For more information see [Network constants](../../../server-constants/network/).

