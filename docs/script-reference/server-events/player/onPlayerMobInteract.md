---
title: 'onPlayerMobInteract'
---
# `event` onPlayerMobInteract <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.4"

This event is triggered when player interacts with any kind of mob object in the world.
In Gothic, mobs are special vobs on the map, that hero can interact with. For example bed, door, chest etc.

## Parameters
```c++
int playerid, int from, int to
```

* `int` **playerid**: the id of the player who interacts with mob.
* `int` **from**: represents previous state of mob. If value is `1`, then mob was used, in any other case value is `0`.
* `int` **to**: represents current state of mob. If value is `1`, then mob is used, in any other case value is `0`.

