---
title: 'onPlayerToggleFaceAni'
---
# `event` onPlayerToggleFaceAni <font size="4">(server-side)</font>
!!! info "Available since version: 0.3.0"

This event is triggered when player face animation is toggled (played or stopped), due to eating or other activities.

## Parameters
```c++
int playerid, string aniName, bool toggle
```

* `int` **playerid**: the id of the player which toggled face animation.
* `string` **aniName**: the face animation name.
* `bool` **toggle**: `true` when player is started playing face animation, otherwise `false`.

