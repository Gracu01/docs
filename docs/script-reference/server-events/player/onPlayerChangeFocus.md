---
title: 'onPlayerChangeFocus'
---
# `event` onPlayerChangeFocus <font size="4">(server-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when player targets another player.

## Parameters
```c++
int playerid, int oldFocusId, int newFocusId
```

* `int` **playerid**: the id of the player which changes the focus.
* `int` **oldFocusId**: the old playerid targeted by the player. Can be `-1` if player wasn't targeting other player.
* `int` **newFocusId**: the new playerid targeted by the player. Can be `-1` if player doesn't target anyone.

