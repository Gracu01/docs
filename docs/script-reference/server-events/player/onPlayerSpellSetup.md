---
title: 'onPlayerSpellSetup'
---
# `event` onPlayerSpellSetup <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.3"

This event is triggered when player prepares the spell.

## Parameters
```c++
int playerid, string|null instance
```

* `int` **playerid**: the id of the player who prepares the spell.
* `string|null` **instance**: the item instance from Daedalus scripts.

