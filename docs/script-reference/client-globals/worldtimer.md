---
title: 'WorldTimer'
---
# `Timestep&` WorldTimer <font size="4">(client-side)</font>

Represents the zengine game world timer instance.  
For more information see [Timestep](../../client-classes/game/Timestep/).