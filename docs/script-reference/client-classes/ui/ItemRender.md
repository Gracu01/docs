---
title: 'ItemRender'
---
# `class` ItemRender <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.7"

This class represents rendered item visible on the screen.

### Constructor
```cpp
ItemRender(int x, int y, int width, int height, string instance)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
* `int` **width**: the width size in virtuals.
* `int` **height**: the height size in virtuals.
* `string` **instance**: the instance of the item to render.

## Properties
### `bool` visible 

Represents the visibility state of the render.

----
### `int` rotX 

Represents the rotation of the item render on the x axis.

----
### `int` rotY 

Represents the rotation of the item render on the y axis.

----
### `int` rotZ 

Represents the rotation of the item render on the z axis.

----
### `int` zbias 

Represents the z bias (Depth Bias) of rendered item model.

----
### `bool` lightingswell 
!!! note
    The game uses it to highlight currently selected item in your inventory.

Represents the highlighting state of the item render.

----
### `string` visual 
!!! info "Available since version: 0.1.9"

Represents the file name of the model used as rendered item visual.

----
### `string` instance 
!!! info "Available since version: 0.1.10"

Represents the instance of rendered item.

----

## Methods
### top

This method will move the item render at the end of the render queue.  
It will be visible at the top of the screen (other elements will be covered by it).

```cpp
void top()
```

  

----
### setPosition

This method will set the render position on screen in virtuals.

```cpp
void setPosition(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
  

----
### setPositionPx

This method will set the render position on screen in pixels.

```cpp
void setPositionPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in pixels.
* `int` **y**: the y position in pixels.
  

----
### setSize

This method will set the render size on screen in virtuals.

```cpp
void setSize(int width, int height)
```

**Parameters:**

* `int` **width**: the width size in virtuals.
* `int` **height**: the height size in virtuals.
  

----
### setSizePx

This method will set the render size on screen in pixels.

```cpp
void setSizePx(int width, int height)
```

**Parameters:**

* `int` **width**: the width size in pixels.
* `int` **height**: the height size in pixels.
  

----
### getPosition

This method will get the render position on screen in virtuals.

```cpp
{x, y} getPosition()
```

  
**Returns `{x, y}`:**

the render position on screen.

----
### getPositionPx

This method will get the render position on screen in pixels.

```cpp
{x, y} getPositionPx()
```

  
**Returns `{x, y}`:**

the render position on screen.

----
### getSize

This method will get the render size on screen in virtuals.

```cpp
{width, height} getSize()
```

  
**Returns `{width, height}`:**

the render size on screen.

----
### getSizePx

This method will get the render size on screen in pixels.

```cpp
{width, height} getSizePx()
```

  
**Returns `{width, height}`:**

the render size on screen.

----
