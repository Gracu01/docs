---
title: 'Draw'
---
# `class` Draw <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This class represents text visible on the screen.

### Constructor
```cpp
Draw(int x, int y, string text)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
* `string` **text**: the text to be shown.

## Properties
### `bool` visible 

Represents the visibility state of the text.

----
### `string` text 

Represents the text displayed by the draw.

----
### `string` font 

Represents the font name used by the text.

----
### `int` alpha 
!!! info "Available since version: 0.1.10"

Represents the text alpha color component in RGBA model.

----
### `float` rotation 
!!! info "Available since version: 0.1.10"
!!! note
    To change that center of rotation, see [setPivotPoint](#setpivotpoint) and [setPivotPointPx](#setpivotpointpx).

Represents the rotation of the text in degrees.  
The rotation is performed clockwise around the draw pivot point.

----
### `int` width <font size="2">(read-only)</font>

Represents the width of the text in virtuals.

----
### `int` widthPx <font size="2">(read-only)</font>

Represents the width of the text in pixels.

----
### `int` height <font size="2">(read-only)</font>

Represents the height of the text in virtuals.

----
### `int` heightPx <font size="2">(read-only)</font>

Represents the height of the text in pixels.

----

## Methods
### top

This method will move the draw text at the end of the render queue.  
It will be visible at the top of the screen (other elements will be covered by it).

```cpp
void top()
```

  

----
### setColor

This method will set the text color.

```cpp
void setColor(int r, int g, int b)
```

**Parameters:**

* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

----
### getColor

This method will get the text color.

```cpp
{r, g, b} getColor()
```

  
**Returns `{r, g, b}`:**

the draw color.

----
### setScale
!!! info "Available since version: 0.1.10"

This method will set the scale of the text.

```cpp
void setScale(float width, float height)
```

**Parameters:**

* `float` **width**: the scale factor on x axis.
* `float` **height**: the scale factor on y axis.
  

----
### getScale
!!! info "Available since version: 0.1.10"

This method will get the scale of the text.

```cpp
{width, height} getScale()
```

  
**Returns `{width, height}`:**

the draw scale.

----
### setPosition

This method will set the text position on screen in virtuals.

```cpp
void setPosition(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in virtuals.
* `int` **y**: the y position in virtuals.
  

----
### getPosition

This method will get the text position on screen in virtuals.

```cpp
{x, y} getPosition()
```

  
**Returns `{x, y}`:**

the draw position on screen.

----
### setPositionPx

This method will set the text position on screen in pixels.

```cpp
void setPositionPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x position in pixels.
* `int` **y**: the y position in pixels.
  

----
### getPositionPx

This method will get the text position on screen in pixels.

```cpp
{x, y} getPositionPx()
```

  
**Returns `{x, y}`:**

the draw position on screen.

----
### setPivotPoint
!!! info "Available since version: 0.1.10"

This method will set the pivot point of the text in virtuals (Offset from the center of the draw).  
All rotations will be done relative to this point.

```cpp
void setPivotPoint(int x, int y)
```

**Parameters:**

* `int` **x**: the x offset in virtuals.
* `int` **y**: the y offset in virtuals.
  

----
### setPivotPointPx
!!! info "Available since version: 0.1.10"

This method will set the pivot point of the text in pixels (Offset from the center of the draw).  
All rotations will be done relative to this point.

```cpp
void setPivotPointPx(int x, int y)
```

**Parameters:**

* `int` **x**: the x offset in pixels.
* `int` **y**: the y offset in pixels.
  

----
### getPivotPoint
!!! info "Available since version: 0.1.10"

This method will get the pivot point of the text in virtuals (Offset from the center of the draw).

```cpp
{x, y} getPivotPoint()
```

  
**Returns `{x, y}`:**

the offset in virtuals. The default pivot point value is `{x=0,y=0}`

----
### getPivotPointPx
!!! info "Available since version: 0.1.10"

This method will get the pivot point of the text in pixels (Offset from the center of the draw).

```cpp
{x, y} getPivotPointPx()
```

  
**Returns `{x, y}`:**

the offset in pixels. The default pivot point value is `{x=0,y=0}`

----
