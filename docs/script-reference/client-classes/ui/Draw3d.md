---
title: 'Draw3d'
---
# `class` Draw3d <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"

This class represents text visible on the screen.  
The text will be shown and follow specified world position, when player is in the distance.

### Constructor
```cpp
Draw3d(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the position in the world on the x axis.
* `float` **y**: the position in the world on the y axis.
* `float` **z**: the position in the world on the z axis.

## Properties
### `bool` visible 

Represents the visibility state of the text.

----
### `string` font 

Represents the font name used by the text.

----
### `int` alpha 
!!! info "Available since version: 0.1.10"

Represents the text alpha color component in RGBA model.

----
### `int` distance 
!!! info "Available since version: 0.1.5.6"

Represents the minimum distance needed between player and draw3d, in order to display it.

----
### `int` width <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the width of the text in virtuals.

----
### `int` widthPx <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the width of the text in pixels.

----
### `int` height <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the height of the text in virtuals.

----
### `int` heightPx <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the height of the text in pixels.

----
### `int` linesCount <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.1"

Represents the number of text lines.

----

## Methods
### top

This method will move the draw text at the end of the render queue.  
It will be visible at the top of the screen (other elements will be covered by it).

```cpp
void top()
```

  

----
### setLineFont
!!! info "Available since version: 0.1.10"

This method will set the line of text alpha channel.

```cpp
void setLineFont(int position, int alpha)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
* `int` **alpha**: the alpha color component in RGBA model.
  

----
### getLineFont
!!! info "Available since version: 0.1.10"

This method will get the line of text alpha channel.

```cpp
int getLineFont(int position)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
  
**Returns `int`:**

the alpha color component in RGBA model.

----
### setColor

This method will set the text color.

```cpp
void setColor(int r, int g, int b)
```

**Parameters:**

* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

----
### getColor

This method will get the text color.

```cpp
{r, g, b} getColor()
```

  
**Returns `{r, g, b}`:**

the draw color.

----
### setLineColor
!!! info "Available since version: 0.1.10"

This method will set the line of text color.

```cpp
void setLineColor(int position, int r, int g, int b)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
* `int` **r**: the red color component in RGB model.
* `int` **g**: the green color component in RGB model.
* `int` **b**: the blue color component in RGB model.
  

----
### getLineColor
!!! info "Available since version: 0.1.10"

This method will get the line of text color.

```cpp
{r, g, b} getLineColor(int position)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
  
**Returns `{r, g, b}`:**

the line of text color.

----
### setLineAlpha
!!! info "Available since version: 0.1.10"

This method will set the line of text alpha channel.

```cpp
void setLineAlpha(int position, int alpha)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
* `int` **alpha**: the alpha color component in RGBA model.
  

----
### getLineAlpha
!!! info "Available since version: 0.1.10"

This method will get the text alpha channel.

```cpp
int getLineAlpha(int position)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
  
**Returns `int`:**

the alpha color component in RGBA model.

----
### insertText

This method will insert new line of text to the draw, moving all previously existing below.

```cpp
void insertText(string text)
```

**Parameters:**

* `string` **text**: the text that will be inserted.
  

----
### removeText

This method will remove line of text from the draw at the specified position, moving up all lines below.

```cpp
void removeText(int position)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
  

----
### getText

This method will get the line text at the specified position.

```cpp
string getText(int position)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
  
**Returns `string`:**

the text at specified position.

----
### setLineText
!!! info "Available since version: 0.1.10"

This method will set line of text at the specified position.

```cpp
void setLineText(int position, string line)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
* `string` **line**: the line of text that will be set.
  

----
### getLineText
!!! info "Available since version: 0.1.10"

This method will get the line of text.

```cpp
string getLineText(int position)
```

**Parameters:**

* `int` **position**: the position of target line in the draw3d.
  
**Returns `string`:**

the line of text.

----
### getPosition
!!! info "Available since version: 0.1.10"

This method will get the text position on screen in virtuals.

```cpp
{x, y} getPosition()
```

  
**Returns `{x, y}`:**

the draw position on screen.

----
### getPositionPx
!!! info "Available since version: 0.1.10"

This method will get the text position on screen in pixels.

```cpp
{x, y} getPositionPx()
```

  
**Returns `{x, y}`:**

the draw position on screen.

----
### setWorldPosition

This method will set the position of the text in the world.

```cpp
void setWorldPosition(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the position in the world on the x axis.
* `float` **y**: the position in the world on the y axis.
* `float` **z**: the position in the world on the z axis.
  

----
### getWorldPosition

This method will get the position of the text in the world.

```cpp
{x, y, z} getWorldPosition()
```

  
**Returns `{x, y, z}`:**

the draw3d world position.

----
