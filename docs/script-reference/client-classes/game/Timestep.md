---
title: 'Timestep'
---
# `class` Timestep <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.9.5"
!!! note
    Use `WorldTimer` global object to work with game time step properties.

This class represents time step object.  
It can be used to measure different time intervals like: frameTime, totalTime.


## Properties
### `float` factorMotion 

Represents the motion factor of a timestep.  
This ration will be used to adjust the timestep values in [reset](#reset) method.

----
### `float` frameTime <font size="2">(read-only)</font>

Represents elapsed time between frames (delta time) in miliseconds since last update.

----
### `float` totalTime <font size="2">(read-only)</font>

Represents elapsed total time in miliseconds.

----
### `float` frameTimeSecs <font size="2">(read-only)</font>

Represents elapsed time between frames (delta time) in seconds since last update.

----
### `float` totalTimeSecs <font size="2">(read-only)</font>

Represents elapsed total time in seconds.

----

## Methods
### reset

This method will update time step values and it will take factor motion into account.  
Call this method in [onRender](../../../client-events/general/onRender/) event, to update time step values by each frame.

```cpp
void reset()
```

  

----
### update
!!! note
    This method will not take into account factor motion multiplier.

This method will update time step values by passed delta time.  
Call this method in [onRender](../../../client-events/general/onRender/) event, to update time step values by each frame.

```cpp
void update(float deltaTime)
```

**Parameters:**

* `float` **deltaTime**: the time between frames in miliseconds.
  

----
