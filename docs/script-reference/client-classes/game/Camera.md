---
title: 'Camera'
---
# `static class` Camera <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.3"

This class represents in game Camera.


## Properties
### `Mat4&` vobMatrix 
!!! info "Available since version: 0.2.0"

Represents the reference to the camera vob matrix in world space.

----
### `Mat4&` viewMatrix 
!!! info "Available since version: 0.2.0"

Represents the reference to the camera view matrix.

----
### `Mat4&` projectionMatrix 
!!! info "Available since version: 0.2.0"

Represents the reference to the camera projection matrix.

----
### `Mat4&` worldMatrix 
!!! info "Available since version: 0.2.0"

Represents the reference to the camera world matrix.

----
### `Mat4&` worldViewMatrix 
!!! info "Available since version: 0.2.0"

Represents the reference to the camera world-view matrix.

----
### `bool` modeChangeEnabled 

Represents the camera mode change feature.  
When mode change is enabled, game switches between different camera modes, e.g: when you draw weapon, camera mode will switch to `"CAMMODMELEE"`.

----
### `bool` movementEnabled 

Represents the camera movement feature.  
When movement is disabled, the camera gets detached from the player.

----

## Methods
### setMode

This method will set the camera mode.

```cpp
void setMode(string mode, array[userpointer] targetList)
```

**Parameters:**

* `string` **mode**: the camera mode.
* `array[userpointer]` **targetList**: =[] the list containing vobs used by the specific camera mode, e.g: CamModDialog requires list containing two npc pointers.
  

----
### getMode

This method will get the camera mode.

```cpp
string getMode()
```

  
**Returns `string`:**

the camera mode.

----
### getPosition

This method will get the camera position.

```cpp
{x, y, z} getPosition()
```

  
**Returns `{x, y, z}`:**

the camera position.

----
### setPosition

This method will set the camera position.

```cpp
void setPosition(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the position on x axis.
* `float` **y**: the position on y axis.
* `float` **z**: the position on z axis.
  

----
### getRotation

This method will get the camera rotation.

```cpp
{x, y, z} getRotation()
```

  
**Returns `{x, y, z}`:**

the camera rotation.

----
### setRotation

This method will set the camera rotation.

```cpp
void setRotation(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the rotation on x axis in deegres.
* `float` **y**: the rotation on y axis in deegres.
* `float` **z**: the rotation on z axis in deegres.
  

----
### project
!!! info "Available since version: 0.1.10"

This method will convert point from 3d world space to screen coordinates (pixels).

```cpp
{x, y} project(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the position on x axis.
* `float` **y**: the position on y axis.
* `float` **z**: the position on z axis.
  
**Returns `{x, y}`:**

screen coordinates represented as pixels. If projected point is behind camera, `null` is returned instead.

----
### backProject
!!! info "Available since version: 0.2.1"

This method will convert point from 2d screen coordinates (pixels) to 3d world space.

```cpp
{x, y, z} backProject(int screenX, int screenY, float distance)
```

**Parameters:**

* `int` **screenX**: the x position on screen in pixels.
* `int` **screenY**: the y position on screen in pixels.
* `float` **distance**: the relative distance from the camera.
  
**Returns `{x, y, z}`:**

world space coordinates.

----
### setTargetVob

This method will set the target vob for the camera.

```cpp
void setTargetVob(Vob vob)
```

**Parameters:**

* `Vob` **vob**: the vob object reference.
  

----
### setTargetPlayer

This method will set the target player for the camera.

```cpp
void setTargetPlayer(int playerId)
```

**Parameters:**

* `int` **playerId**: the id of the player.
  

----
