---
title: 'TriggerBase'
---
# `class` TriggerBase `extends` [Vob](../Vob/) <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This class represents world trigger list, which can activate various things.

### Constructor
```cpp
TriggerBase()
```

**Parameters:**

No parameters.
### Constructor
```cpp
TriggerBase(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `int` listProcess 

Represents how the targets will be processed in the trigger list. For more information see [TriggerList Process types](../../../client-constants/vob/triggerlist-process).

----
### `int` actTarget 

Represents the index of the next target in the trigger list which will invoke any trigger event.

----
### `bool` sendOnTrigger 

Represents the state which determines if next invoked event will be trigger (true) or untrigger (false).

----

## Methods
### getTarget

This method will get the vob name at specified index that will be triggered by the trigger list.

```cpp
string getTarget(int index)
```

**Parameters:**

* `int` **index**: the index of the target name in the trigger list (in range between [0, 5]).
  
**Returns `string`:**



----
### getFireDelay

This method will get the delay time in seconds at specified index in which the trigger list will invoke any trigger event.

```cpp
float getFireDelay(int index)
```

**Parameters:**

* `int` **index**: the index of delay time in the trigger list (in range between [0, 5]).
  
**Returns `float`:**



----
### setTarget

This method will set the vob name at specified index that will be triggered by the trigger list.

```cpp
void setTarget(int index, string name)
```

**Parameters:**

* `int` **index**: the index of the target name in the trigger list (in range between [0, 5]).
* `string` **name**: the name of the vob that will be triggered by the trigger.
  

----
### setFireDelay

This method will set the delay time in seconds at specified index in which the trigger list will invoke any trigger event.

```cpp
void setFireDelay(int index, float fireDelay)
```

**Parameters:**

* `int` **index**: the index of the delay time in the trigger list (in range between [0, 5]).
* `float` **fireDelay**: the delay time in seconds in which the trigger event will be invoked.
  

----
### triggerActTarget
!!! note
    In order to invoke this action trigger must be added to game world.

This method will trigger the next target vob in the trigger list.

```cpp
void triggerActTarget(userpointer instigatorVob = null)
```

**Parameters:**

* `userpointer` **instigatorVob**: the vob that is used by some of engine classes. Can be ommited in most cases.
  

----
