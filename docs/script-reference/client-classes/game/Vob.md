---
title: 'Vob'
---
# `class` Vob <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"
!!! note
    By default the vob is not added to the world. To do that use [addToWorld](#addtoworld).

This class represents 3d object in the world.

### Constructor
```cpp
Vob(string model)
```

**Parameters:**

* `string` **model**: the model name to be used as visual.
### Constructor
```cpp
Vob(userpointer ptr)
```

**Parameters:**

* `userpointer` **ptr**: the pointer to the object from the game.

## Properties
### `string` objectName 
!!! info "Available since version: 0.2.0"

Represents the internal engine object name.

----
### `Mat4&` matrix 
!!! info "Available since version: 0.2.0"

Represents the reference to the vob matrix.

----
### `string` sectorName <font size="2">(read-only)</font>
!!! info "Available since version: 0.3.0"

Represents the name of the sector in which vob is currently located.

----
### `bool` cdDynamic 
!!! info "Available since version: 0.1.10"

Represents the state of dynamic collision of vob.  
Enabling this option will prevent other dynamic objects (i.e. player) from passing through it.

----
### `bool` cdStatic 
!!! info "Available since version: 0.1.10"

Represents the state of static collision of vob.  
Enabling this option will prevent static objects (i.e. world mesh) from passing through it.

----
### `float` farClipZScale 
!!! info "Available since version: 0.1.10"

Represents the max distance at which the vob will still be rendered.

----
### `string` visual 
!!! info "Available since version: 0.1.10"

Represents the model file name used as vob visual, e.g `SPHERE.3DS`.

----
### `float` visualAlpha 
!!! info "Available since version: 0.1.10"
!!! note
    The alpha must be in range between `0.0` (fully transparent) to `1.0` (fully visible).

Represents the transparency of the vob visual.

----
### `bool` sleeping 
!!! info "Available since version: 0.2.0"

Represents the vob sleeping state. While vob is in sleeping state,
systems like AI, animations, physics are disabled.

----
### `bool` physicsEnabled 
!!! info "Available since version: 0.2.0"

Represents the state of flag about vob physics.

----
### `bool` drawBBox3d 
!!! info "Available since version: 0.2.0"

Represents the state of flag about drawing bounding box 3d of vob.

----
### `bool` castDynShadow 
!!! info "Available since version: 0.2.0"

Represents the state of flag about rendering dynamic shadow for vob.

----
### `bool` ignoredByTraceRay 
!!! info "Available since version: 0.2.0"

Represents the state of flag about ignoring vob in trace ray.

----
### `bool` isProjectile <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.0"

Checks whether vob is a projectile.

----
### `bool` isInWater <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.0"

Checks whether vob is in water.

----
### `Vec3` velocity <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.0"

Represents current vob velocity measured in cm/seconds.

----
### `BBox3d&` bbox3dWorld 
!!! info "Available since version: 0.2.0"

Represents the bounding box 3d of vob, shifted by its position in world.

----
### `BBox3d` bbox3dLocal 
!!! info "Available since version: 0.2.0"

Represents the bounding box 3d of vob.

----
### `bool` isInMovement <font size="2">(read-only)</font>
!!! info "Available since version: 0.2.0"

Checks whether vob is in movement transaction.

----
### `userpointer` ptr <font size="2">(read-only)</font>
!!! info "Available since version: 0.1.10"

Represents the pointer to the object from the game.

----

## Methods
### beginMovement
!!! info "Available since version: 0.2.0"

This method will start movement transaction. Transaction can be started to only one vob at a time!
While transaction is active nothing related to physics or collisions can be changed.

```cpp
void beginMovement()
```

  

----
### endMovement
!!! info "Available since version: 0.2.0"

This method will close movement transaction.
After closing transaction some internal checks are runned, like collisions.

```cpp
void endMovement()
```

  

----
### setHeadingAtWorld
!!! info "Available since version: 0.2.0"

This method will set heading direction of vob.

```cpp
void setHeadingAtWorld(Vec3 direction)
```

**Parameters:**

* `Vec3` **direction**: the heading direction.
  

----
### getTrafoModelNodeToWorld
!!! info "Available since version: 0.2.0"

This method will get world matrix for selected node from visual eg. "ZS_RIGHTHAND".
In case when provided node name does not exist, vob world matrix is returned instead.

```cpp
Mat4 getTrafoModelNodeToWorld(string node)
```

**Parameters:**

* `string` **node**: the node name.
  
**Returns `Mat4`:**

the node world matrix.

----
### setPosition

This method will set the position of the vob in the world.

```cpp
void setPosition(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the position in the world on the x axis.
* `float` **y**: the position in the world on the y axis.
* `float` **z**: the position in the world on the z axis.
  

----
### getPosition

This method will get the position of the vob in the world.

```cpp
{x, y, z} getPosition()
```

  
**Returns `{x, y, z}`:**

the vob world position.

----
### setRotation

This method will set the euler rotation of the vob in the world.

```cpp
void setRotation(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the rotation of the vob on the x axis.
* `float` **y**: the rotation of the vob on the y axis.
* `float` **z**: the rotation of the vob on the z axis.
  

----
### setRotation
!!! info "Available since version: 0.2.1"

This method will set the quaternion rotation of the vob in the world.

```cpp
void setRotation(Quat q)
```

**Parameters:**

* `Quat` **q**: the rotation of the vob in form of quaternion.
  

----
### getRotation

This method will get the euler rotation of the vob in the world.

```cpp
{x, y, z} getRotation()
```

  
**Returns `{x, y, z}`:**

the vob world rotation.

----
### addToWorld
!!! info "Available since version: 0.1.10"

This method will add the vob to currently loaded world.

```cpp
void addToWorld()
```

  

----
### removeFromWorld
!!! info "Available since version: 0.1.10"

This method will remove the vob from currently loaded world.

```cpp
void removeFromWorld()
```

  

----
### floor
!!! info "Available since version: 0.1.10"

This method will try to put the vob on floor.  
If the difference between vob position and the floor y position is less or equal 1000, the method will succeed.

```cpp
void floor()
```

  

----
