---
title: 'DaedalusSymbol'
---
# `static class` DaedalusSymbol <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This class represents Daedalus parser symbol. Symbol is just a unit daedalus scripting langauge, e.g: function, class, instance, prototype, variable, constant, etc.


## Properties
### `string` name 

Represents name of the symbol.

----
### `DaedalusSymbol|null` parent 

Represents the parent symbol of current symbol.

----
### `DaedalusSymbol|null` next 

Represents the next symbol in linked list of symbols.

----
### `int` ele 

Represents the the number of elements inside a symbol. If symbol isn't array it will return 1, otherwise it will return the size of an array.

----
### `int` type 

Represents the the type of a symbol. For more information see [Daedalus Types](../../../client-constants/daedalustype/)

----
### `int` flags 

Represents the the flags of a symbol. For more information see [Daedalus Flags](../../../client-constants/daedalusflags/)

----
### `bool` space 

Holds boolean value that indicates whether or not symbol memory was allocated.

----

## Methods
### getValue
!!! note
    Only ints, floats and strings are supported.

This method will get the symbol value.

```cpp
int|float|string getValue(int arrayIndex = 0)
```

**Parameters:**

* `int` **arrayIndex**: the symbol array index.
  
**Returns `int|float|string`:**

the value of the symbol supported by the daedalus scripting language.

----
### setValue
!!! note
    Only ints, floats and strings are supported.

This method will set the symbol value.

```cpp
void setValue(int|float|string value, int arrayIndex = 0)
```

**Parameters:**

* `int|float|string` **value**: the symbol value.
* `int` **arrayIndex**: the symbol array index.
  

----
