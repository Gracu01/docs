---
title: 'DiscordActivityParty'
---
# `static class` DiscordActivityParty <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents Discord activity assets which cane be added on the developer website.


## Properties
### `DiscordPartySize` size 

Represents information about the size of the party.

----
### `string` id 
!!! note
    Maximum length of text is 128 characters!

Represents a unique identifier for this party.

----

## Methods
No methods.

----
