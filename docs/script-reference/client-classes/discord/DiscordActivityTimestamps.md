---
title: 'DiscordActivityTimestamps'
---
# `class` DiscordActivityTimestamps <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents Discord activity timestamps.


## Properties
### `int` start 

Represents unix timestamp of when application started. After setting this value activity will display "*elapsed*" timer.

----
### `int` end 

Represents unix timestamp of remaining time. After setting this value activity will display "*remaining*" timer.

----

## Methods
No methods.

----
