---
title: 'DiscordActivityAssets'
---
# `class` DiscordActivityAssets <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents Discord application assets used in displayed activity.


## Properties
### `string` largeImage 
!!! note
    Maximum length of key is 128 characters!

Represents large image key for displayed activity.

----
### `string` largeText 
!!! note
    Maximum length of text is 128 characters!

Represents text displayed on large image for activity.

----
### `string` smallImage 
!!! note
    Maximum length of key is 128 characters!

Represents small image key for displayed activity.

----
### `string` smallText 
!!! note
    Maximum length of text is 128 characters!

Represents text displayed on small image for activity.

----

## Methods
No methods.

----
