---
title: 'DiscordPartySize'
---
# `class` DiscordPartySize <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.0"

This class represents Discord information about the size of the party.


## Properties
### `int` current 

Represents current size of party.

----
### `int` max 

Represents max possible size of the party.

----

## Methods
No methods.

----
