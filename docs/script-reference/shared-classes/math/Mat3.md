---
title: 'Mat3'
---
# `class` Mat3 <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents 3x3 matrix.

### Constructor
```cpp
Mat3()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Mat3(float value)
```

**Parameters:**

* `float` **value**: the initial value for each matrix cell.
### Constructor
```cpp
Mat3(Vec3 v0, Vec3 v1, Vec3 v2)
```

**Parameters:**

* `Vec3` **v0**: the initial value for 1st row of the matrix.
* `Vec3` **v1**: the initial value for 2nd row of the matrix.
* `Vec3` **v2**: the initial value for 3rd row of the matrix.

## Properties
No properties.

----

## Methods
### makeIdentity

This method will set all cells to the identity matrix.

```cpp
void makeIdentity()
```

  

----
### makeZero

This method will set all cells to zero.

```cpp
void makeZero()
```

  

----
### makeOrthonormal

This method will make the matric orthonormal.

```cpp
void makeOrthonormal()
```

  

----
### isUpper3x3Orthonormal

This method will is used check whether upper 3x3 matrix components are orthonormal.

```cpp
bool isUpper3x3Orthonormal()
```

  
**Returns `bool`:**

`true` if upper 3x3 components are orthonormal, otherwise `false`.

----
### transpose

This method will return the transpose of the matrix.  
The transposed matrix is the one that has the Matrix4x4's columns exchanged with its rows.

```cpp
Mat3 transpose()
```

  
**Returns `Mat3`:**

the transposed matrix.

----
### inverse

This method will return the inverse of the matrix.  
Inverted matrix is such that if multiplied by the original would result in identity matrix.

```cpp
Mat3 inverse()
```

  
**Returns `Mat3`:**

the inverted matrix.

----
### rotate

This method will apply the matrix rotation to given vector and return a new one.

```cpp
Vec3 rotate(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the vector that will be used to produce the rotated one.
  
**Returns `Vec3`:**

the rotated vector.

----
### getRightVector

This method will get the right vector from the matrix.

```cpp
Vec3 getRightVector()
```

  
**Returns `Vec3`:**

the right vector.

----
### setRightVector

This method will set the right vector for the matrix.

```cpp
void setRightVector(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the new right vector for the matrix.
  

----
### getUpVector

This method will get the up vector from the matrix.

```cpp
Vec3 getUpVector()
```

  
**Returns `Vec3`:**

the up vector.

----
### setUpVector

This method will set the up vector for the matrix.

```cpp
void setUpVector(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the new up vector for the matrix.
  

----
### getAtVector

This method will get the at vector from the matrix.

```cpp
Vec3 getAtVector()
```

  
**Returns `Vec3`:**

the at vector.

----
### setAtVector

This method will set the at vector for the matrix.

```cpp
void setAtVector(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the new at vector for the matrix.
  

----
### resetRotation

This method will reset the rotation of the matrix.

```cpp
void resetRotation()
```

  

----
### extractRotation

This method will extract the rotation matrix from the TRS matrix.

```cpp
Mat3 extractRotation()
```

  
**Returns `Mat3`:**

the extracted rotation matrix.

----
### extractScaling

This method will extract the scaling vector from the TRS matrix.

```cpp
Vec3 extractScaling()
```

  
**Returns `Vec3`:**

the extracted scaling vector.

----
### postRotateX

This method will perform the matrix post rotation operation on X axis.

```cpp
void postRotateX(float angle)
```

**Parameters:**

* `float` **angle**: the angle in degrees.
  

----
### postRotateY

This method will perform the matrix post rotation operation on Y axis.

```cpp
void postRotateY(float angle)
```

**Parameters:**

* `float` **angle**: the angle in degrees.
  

----
### postRotateZ

This method will perform the matrix post rotation operation on Z axis.

```cpp
void postRotateZ(float angle)
```

**Parameters:**

* `float` **angle**: the angle in degrees.
  

----
### preScale

This method will perform the matrix pre scale operation.

```cpp
void preScale(Vec3 scale)
```

**Parameters:**

* `Vec3` **scale**: the scale vector.
  

----
### postScale

This method will perform the matrix post scale operation.

```cpp
void postScale(Vec3 scale)
```

**Parameters:**

* `Vec3` **scale**: the scale vector.
  

----
### `static` swap

This method will swap all cells between two matricies.

```cpp
void swap(Mat3 mat1, Mat3 mat2)
```

**Parameters:**

* `Mat3` **mat1**: the matrix that will be used in swap operation.
* `Mat3` **mat2**: the matrix that will be used in swap operation.
  

----
