---
title: 'Mat4'
---
# `class` Mat4 <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents 4x4 matrix.

### Constructor
```cpp
Mat4()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Mat4(float value)
```

**Parameters:**

* `float` **value**: the initial value for each matrix cell.
### Constructor
```cpp
Mat4(Vec4 v0, Vec4 v1, Vec4 v2, Vec4 v3)
```

**Parameters:**

* `Vec4` **v0**: the initial value for 1st row of the matrix.
* `Vec4` **v1**: the initial value for 2nd row of the matrix.
* `Vec4` **v2**: the initial value for 3rd row of the matrix.
* `Vec4` **v3**: the initial value for 4th row of the matrix.

## Properties
No properties.

----

## Methods
### makeIdentity

This method will set all cells to the identity matrix.

```cpp
void makeIdentity()
```

  

----
### makeZero

This method will set all cells to zero.

```cpp
void makeZero()
```

  

----
### makeOrthonormal

This method will make the matric orthonormal.

```cpp
void makeOrthonormal()
```

  

----
### isUpper3x3Orthonormal

This method will is used check whether upper 3x3 matrix components are orthonormal.

```cpp
bool isUpper3x3Orthonormal()
```

  
**Returns `bool`:**

`true` if upper 3x3 components are orthonormal, otherwise `false`.

----
### transpose

This method will return the transpose of the matrix.
The transposed matrix is the one that has the Matrix4x4's columns exchanged with its rows.

```cpp
Mat4 transpose()
```

  
**Returns `Mat4`:**

the transposed matrix.

----
### inverse

This method will return the inverse of the matrix.
Inverted matrix is such that if multiplied by the original would result in identity matrix.

```cpp
Mat4 inverse()
```

  
**Returns `Mat4`:**

the inverted matrix.

----
### inverseLinTrafo

This method will return the inverse linear transformation of the matrix.
It works almost exactly the same as [inverse](#inverse)  
with the difference that translation is applied.

```cpp
Mat4 inverseLinTrafo()
```

  
**Returns `Mat4`:**

the inverted matrix.

----
### rotate

This method will apply the matrix rotation to given vector and return a new one.

```cpp
Vec3 rotate(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the vector that will be used to produce the rotated one.
  
**Returns `Vec3`:**

the rotated vector.

----
### getRightVector

This method will get the right vector from the matrix.

```cpp
Vec3 getRightVector()
```

  
**Returns `Vec3`:**

the right vector.

----
### setRightVector

This method will set the right vector for the matrix.

```cpp
void setRightVector(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the new right vector for the matrix.
  

----
### getAtVector

This method will get the at vector from the matrix.

```cpp
Vec3 getAtVector()
```

  
**Returns `Vec3`:**

the at vector.

----
### setAtVector

This method will set the at vector for the matrix.

```cpp
void setAtVector(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the new at vector for the matrix.
  

----
### getUpVector

This method will get the up vector from the matrix.

```cpp
Vec3 getUpVector()
```

  
**Returns `Vec3`:**

the up vector.

----
### setUpVector

This method will set the up vector for the matrix.

```cpp
void setUpVector(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the new up vector for the matrix.
  

----
### getTranslation
!!! info "Available since version: 0.2.1"

This method will get the translation (aka position) from the matrix.

```cpp
Vec3 getTranslation()
```

  
**Returns `Vec3`:**

the translation position.

----
### setTranslation
!!! info "Available since version: 0.2.1"

This method will set the translation (aka position) for the matrix.

```cpp
void setTranslation(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the translation position.
  

----
### resetRotation

This method will reset the rotation of the matrix.

```cpp
void resetRotation()
```

  

----
### extractRotation

This method will extract the rotation matrix from the TRS matrix.

```cpp
Mat3 extractRotation()
```

  
**Returns `Mat3`:**

the extracted rotation matrix.

----
### extractScaling

This method will extract the scaling vector from the TRS matrix.

```cpp
Vec3 extractScaling()
```

  
**Returns `Vec3`:**

the extracted scaling vector.

----
### postRotateX

This method will perform the matrix post rotation operation on X axis.

```cpp
void postRotateX(float angle)
```

**Parameters:**

* `float` **angle**: the angle in degrees.
  

----
### postRotateY

This method will perform the matrix post rotation operation on Y axis.

```cpp
void postRotateY(float angle)
```

**Parameters:**

* `float` **angle**: the angle in degrees.
  

----
### postRotateZ

This method will perform the matrix post rotation operation on Z axis.

```cpp
void postRotateZ(float angle)
```

**Parameters:**

* `float` **angle**: the angle in degrees.
  

----
### preScale

This method will perform the matrix pre scale operation.

```cpp
void preScale(Vec3 scale)
```

**Parameters:**

* `Vec3` **scale**: the scale vector.
  

----
### postScale

This method will perform the matrix post scale operation.

```cpp
void postScale(Vec3 scale)
```

**Parameters:**

* `Vec3` **scale**: the scale vector.
  

----
### `static` swap

This method will swap all cells between two matricies.

```cpp
void swap(Mat4 mat1, Mat4 mat2)
```

**Parameters:**

* `Mat4` **mat1**: the matrix that will be used in swap operation.
* `Mat4` **mat2**: the matrix that will be used in swap operation.
  

----
### `static` lookAt
!!! info "Available since version: 0.2.1"

This method will create a matrix that will hold position & rotation based of from, to, up vectors.

```cpp
Mat4 lookAt(Vec3 from, Vec3 to, Vec3 up)
```

**Parameters:**

* `Vec3` **from**: the source position.
* `Vec3` **to**: the target position.
* `Vec3` **up**: the up direction.
  
**Returns `Mat4`:**

the resulting transformation matrix

----
