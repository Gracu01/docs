---
title: 'Vec3'
---
# `class` Vec3 <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents 3d vector.

### Constructor
```cpp
Vec3()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Vec3(float value)
```

**Parameters:**

* `float` **value**: the initial value for x, y, z components.
### Constructor
```cpp
Vec3(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the initial value for x component.
* `float` **y**: the initial value for y component.
* `float` **z**: the initial value for z component.

## Properties
### `float` x 

Represents the x component of the vector.

----
### `float` y 

Represents the y component of the vector.

----
### `float` z 

Represents the z component of the vector.

----

## Methods
### len

This method will return the vector length.
The length of the vector is square root of `(x*x + y*y + z*z)`.

```cpp
float len()
```

  
**Returns `float`:**

the squared length of the vector.

----
### len2

This method will get the squared vector length.
The length of the vector is `(x*x + y*y + z*z)`.
Calculating the squared length instead of the [len](#len) is much faster.

```cpp
float len2()
```

  
**Returns `float`:**

the length of the vector.

----
### lenApprox

This method will get the approximate vector length.
The length of the vector is approximation square root of `(x*x + y*y + z*z)`.
This method is faster than [len](#len) but less accurate.

```cpp
float lenApprox()
```

  
**Returns `float`:**

the length of the vector.

----
### distance

This method will get 3d distance between two vectors.

```cpp
float distance(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the vector that will be used in distance calculation.
  
**Returns `float`:**

the 3d distance between vectors.

----
### distance2d

This method will get 2d distance between two vectors.

```cpp
float distance2d(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the vector that will be used in distance calculation.
  
**Returns `float`:**

the 2d distance between vectors.

----
### normalize
!!! note
    Avoid normalizing a vector with length = `0`.

This method will normalize the vector.
Normalizing a vector will cause it's length to be equal to `1`.
The direction of the vector won't be affected.

```cpp
Vec3& normalize()
```

  
**Returns `Vec3&`:**



----
### normalizeSafe

This method will normalize the vector.
It works almost exactly the same as [normalize](#normalize)  
with the difference that zero length vector won't cause any issues.

```cpp
Vec3& normalizeSafe()
```

  
**Returns `Vec3&`:**



----
### normalizeApprox
!!! note
    Avoid normalizing a vector with length = `0`.

This method will normalize the vector with small approximation.
It works almost the same as [normalize](#normalize), but it's faster and less accurate.
It uses the `fast inverse square root` algorithm as the base of calculating square root.

```cpp
Vec3& normalizeApprox()
```

  
**Returns `Vec3&`:**



----
### set

This method will set all of the vector components.

```cpp
void set(float x, float y, float z)
```

**Parameters:**

* `float` **x**: the new vector x component.
* `float` **y**: the new vector y component.
* `float` **z**: the new vector z component.
  

----
### isEqualEps

This method will return `true` if vectors are approximately the same.
The minimal difference used for comparison is `0.001`.

```cpp
bool isEqualEps(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the vector that will be used in comparision.
  
**Returns `bool`:**

`true` if vectors are approximately the same, otherwise `false`.

----
### abs

This method will return vector with absolute x,y,z components.

```cpp
Vec3 abs()
```

  
**Returns `Vec3`:**

the new vector result.

----
### reflect

This method will return a reflected vector off normal plane.

```cpp
Vec3 reflect(Vec3 normal)
```

**Parameters:**

* `Vec3` **normal**: the normal plane that defines a plane.
  
**Returns `Vec3`:**

the new vector result.

----
### `static` swap

This method will swap x,y,z components between two vectors.

```cpp
void swap(Vec3 vec1, Vec3 vec2)
```

**Parameters:**

* `Vec3` **vec1**: the vector that will be used in swap operation.
* `Vec3` **vec2**: the vector that will be used in swap operation.
  

----
### `static` min

This method will return vector with the smallest x,y,z components from two vectors.

```cpp
Vec3 min(Vec3 vec1, Vec3 vec2)
```

**Parameters:**

* `Vec3` **vec1**: the vector that will be used in min operation.
* `Vec3` **vec2**: the vector that will be used in min operation.
  
**Returns `Vec3`:**

the new vector result.

----
### `static` max

This method will return vector with the largest x,y,z components from two vectors.

```cpp
Vec3 max(Vec3 vec1, Vec3 vec2)
```

**Parameters:**

* `Vec3` **vec1**: the vector that will be used in max operation.
* `Vec3` **vec2**: the vector that will be used in max operation.
  
**Returns `Vec3`:**

the new vector result.

----
### `static` prod

This method will multiply two vectors and return the result as a new vector.

```cpp
Vec3 prod(Vec3 vec1, Vec3 vec2)
```

**Parameters:**

* `Vec3` **vec1**: the vector that will be used in prod operation.
* `Vec3` **vec2**: the vector that will be used in prod operation.
  
**Returns `Vec3`:**

the new vector result.

----
### `static` dot

This method will return the dot product between two vectors.
For normalized vectors dot returns:
- `1` if they point in exactly the same direction
- `-1` if they point in completely opposite directions
and a number in between for other cases (e.g. Dot returns zero if vectors are perpendicular).
For vectors of arbitrary length the Dot return values are similar: they get larger when the angle between vectors decreases.

```cpp
float dot(Vec3 vec1, Vec3 vec2)
```

**Parameters:**

* `Vec3` **vec1**: the vector that will be used in dot operation.
* `Vec3` **vec2**: the vector that will be used in dot operation.
  
**Returns `float`:**

the dot product.

----
### `static` cross

This method will return the cross product between two vectors.  
The cross product of two vectors results in a third vector which is perpendicular to the two input vectors.  
The result's magnitude is equal to the magnitudes of the two inputs multiplied together and then multiplied by the sine of the angle between the inputs.

```cpp
Vec3 cross(Vec3 vec1, Vec3 vec2)
```

**Parameters:**

* `Vec3` **vec1**: the vector that will be used in cross operation.
* `Vec3` **vec2**: the vector that will be used in cross operation.
  
**Returns `Vec3`:**

the vector cross product.

----
### `static` lerp

This method will return interpolated vector between vectors a and b by ratio t.

```cpp
Vec3 lerp(float t, Vec3 v1, Vec3 v2)
```

**Parameters:**

* `float` **t**: interpolation ratio.
* `Vec3` **v1**: the vector used in lerp operation.
* `Vec3` **v2**: the vector used in lerp operation.
  
**Returns `Vec3`:**

the lerp vector result.

----
