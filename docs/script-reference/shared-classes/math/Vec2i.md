---
title: 'Vec2i'
---
# `class` Vec2i <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents 2d vector using integers.

### Constructor
```cpp
Vec2i()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Vec2i(int value)
```

**Parameters:**

* `int` **value**: the initial value for x, y components.
### Constructor
```cpp
Vec2i(int x, int y)
```

**Parameters:**

* `int` **x**: the initial value for x component.
* `int` **y**: the initial value for y component.

## Properties
### `int` x 

Represents the x component of the vector.

----
### `int` y 

Represents the y component of the vector.

----

## Methods
### len

This method will return the vector length.
The length of the vector is square root of `(x*x + y*y)`.

```cpp
float len()
```

  
**Returns `float`:**

the squared length of the vector.

----
### len2

This method will get the squared vector length.
The length of the vector is `(x*x + y*y)`.
Calculating the squared length instead of the [len](#len) is much faster.

```cpp
int len2()
```

  
**Returns `int`:**

the length of the vector.

----
### lenApprox

This method will get the approximate vector length.
The length of the vector is approximation square root of `(x*x + y*y)`.
This method is faster than [len](#len) but less accurate.

```cpp
int lenApprox()
```

  
**Returns `int`:**

the length of the vector.

----
### distance

This method will get distance between two vectors.

```cpp
float distance(Vec2i vec)
```

**Parameters:**

* `Vec2i` **vec**: the vector that will be used in distance calculation.
  
**Returns `float`:**

the distance between vectors.

----
### set

This method will set all of the vector components.

```cpp
void set(int x, int y)
```

**Parameters:**

* `int` **x**: the new vector x component.
* `int` **y**: the new vector y component.
  

----
### abs

This method will return vector with absolute x,y components.

```cpp
Vec2i abs()
```

  
**Returns `Vec2i`:**

the new vector result.

----
### `static` swap

This method will swap x,y components between two vectors.

```cpp
void swap(Vec2i vec1, Vec2i vec2)
```

**Parameters:**

* `Vec2i` **vec1**: the vector that will be used in swap operation.
* `Vec2i` **vec2**: the vector that will be used in swap operation.
  

----
### `static` min

This method will return vector with the smallest x,y components from two vectors.

```cpp
Vec2i min(Vec2i vec1, Vec2i vec2)
```

**Parameters:**

* `Vec2i` **vec1**: the vector that will be used in min operation.
* `Vec2i` **vec2**: the vector that will be used in min operation.
  
**Returns `Vec2i`:**

the new vector result.

----
### `static` max

This method will return vector with the largest x,y components from two vectors.

```cpp
Vec2i max(Vec2i vec1, Vec2i vec2)
```

**Parameters:**

* `Vec2i` **vec1**: the vector that will be used in max operation.
* `Vec2i` **vec2**: the vector that will be used in max operation.
  
**Returns `Vec2i`:**

the new vector result.

----
### `static` prod

This method will multiply two vectors and return the result as a new vector.

```cpp
Vec2i prod(Vec2i vec1, Vec2i vec2)
```

**Parameters:**

* `Vec2i` **vec1**: the vector that will be used in prod operation.
* `Vec2i` **vec2**: the vector that will be used in prod operation.
  
**Returns `Vec2i`:**

the new vector result.

----
