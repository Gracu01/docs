---
title: 'Quat'
---
# `class` Quat <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This class represents quaternion.

### Constructor
```cpp
Quat()
```

**Parameters:**

No parameters.
### Constructor
```cpp
Quat(float w)
```

**Parameters:**

* `float` **w**: the initial value for w component, x, y, z components will be set to `0`.
### Constructor
```cpp
Quat(float x, float y, float z, float w)
```

**Parameters:**

* `float` **x**: the initial value for x component.
* `float` **y**: the initial value for y component.
* `float` **z**: the initial value for z component.
* `float` **w**: the initial value for w component.

## Properties
### `float` x 

Represents the x component of the quaternion.

----
### `float` y 

Represents the y component of the quaternion.

----
### `float` z 

Represents the z component of the quaternion.

----
### `float` w 

Represents the w component of the quaternion.

----

## Methods
### toMat3

This method will return rotation represented by the 3x3 matrix.

```cpp
Mat3 toMat3()
```

  
**Returns `Mat3`:**

the 3x3 rotation matrix.

----
### fromMat3

This method will set quaternion rotation from the 3x3 matrix rotation representation.

```cpp
void fromMat3(Mat3 mat)
```

**Parameters:**

* `Mat3` **mat**: the 3x3 rotation matrix.
  

----
### toMat4

This method will return rotation represented by the 4x4 matrix.

```cpp
Mat4 toMat4()
```

  
**Returns `Mat4`:**

the 4x4 rotation matrix.

----
### fromMat4

This method will set quaternion rotation from the 4x4 matrix rotation representation.

```cpp
void fromMat4(Mat4 mat)
```

**Parameters:**

* `Mat4` **mat**: the 4x4 rotation matrix.
  

----
### toEuler

This method will return rotation represented in euler angles (radians).

```cpp
Vec3 toEuler()
```

  
**Returns `Vec3`:**

the vector that stores euler angles in radians.

----
### fromEuler

This method will set quaternion rotation from the euler angles representation (radians).

```cpp
void fromEuler(Vec3 vec)
```

**Parameters:**

* `Vec3` **vec**: the vector that stores euler angles in radians.
  

----
### toAxisAngle

This method will return axis rotation as a Vec4.  
The x,y,z components will be set to axis vector, while w will represent the angle in radians.

```cpp
Vec4 toAxisAngle()
```

  
**Returns `Vec4`:**

the vector that stores axis + angle in radians.

----
### fromAxisAngle

This method will set quaternion rotation from the axis angle representation.

```cpp
void fromAxisAngle(Vec3 axis, float angle)
```

**Parameters:**

* `Vec3` **axis**: the vector that represents the 3d axis.
* `float` **angle**: the angle of the axis rotation in radians.
  

----
### makeIdentity

This method will set x, y, z, w components to the identity quaternion.

```cpp
void makeIdentity()
```

  

----
### isIdentity

This method will is used check whether the x, y, z, w components are set to identity quaternion.

```cpp
bool isIdentity()
```

  
**Returns `bool`:**

`true` if quaternion components are set to identity quaternion, otherwise `false`.

----
### len

This method will return the quaternion length.
The length of the quaternion is square root of `(x*x + y*y + z*z + w*w)`.

```cpp
float len()
```

  
**Returns `float`:**

the squared length of the quaternion.

----
### len2

This method will get the squared quaternion length.
The length of the quaternion is `(x*x + y*y + z*z + w*w)`.
Calculating the squared length instead of the [len](#len) is much faster.

```cpp
float len2()
```

  
**Returns `float`:**

the length of the vector.

----
### lenApprox

This method will get the approximate quaternion length.
The length of the quaternion is approximation square root of `(x*x + y*y + z*z)`.
This method is faster than [len](#len) but less accurate.

```cpp
float lenApprox()
```

  
**Returns `float`:**

the length of the quaternion.

----
### normalize
!!! note
    Avoid normalizing a quaternion with length = `0`.

This method will normalize the quaternion.  
Normalizing a quaternion will cause it's length to be equal to `1`.
The orientation of the quaternion won't be affected.

```cpp
Quat& normalize()
```

  
**Returns `Quat&`:**



----
### normalizeSafe

This method will normalize the quaternion.
It works almost exactly the same as [normalize](#normalize)
with the difference that zero length quaternion won't cause any issues.

```cpp
Quat& normalizeSafe()
```

  
**Returns `Quat&`:**



----
### normalizeApprox
!!! note
    Avoid normalizing a quaternion with length = `0`.

This method will normalize the quaternion with small approximation.
It works almost the same as [normalize](#normalize), but it's faster and less accurate.
It uses the `fast inverse square root` algorithm as the base of calculating square root.

```cpp
Quat& normalizeApprox()
```

  
**Returns `Quat&`:**



----
### set

This method will set all of the quaternion components.

```cpp
void set(float x, float y, float z, float w)
```

**Parameters:**

* `float` **x**: the new quaternion x component.
* `float` **y**: the new quaternion y component.
* `float` **z**: the new quaternion z component.
* `float` **w**: the new quaternion z component.
  

----
### inverse

This method will return the inverse of rotation of the quaternion.

```cpp
Quat inverse()
```

  
**Returns `Quat`:**

the inverted quaternion.

----
### conjugate

This method will return the conjugate of the quaternion.  
The conjugated quaternion will have the x, y, z components negated from original quaternion.

```cpp
Quat conjugate()
```

  
**Returns `Quat`:**

the conjugated quaternion.

----
### `static` dot

This method will return the dot product between two quaternions.

```cpp
float dot(Quat quat1, Quat quat2)
```

**Parameters:**

* `Quat` **quat1**: the quaternion that will be used in dot operation.
* `Quat` **quat2**: the quaternion that will be used in dot operation.
  
**Returns `float`:**

the dot product.

----
### `static` lerp

This method will return interpolated quaternion between quaternions a and b by ratio t.

```cpp
Quat lerp(float t, Quat q1, Quat q2)
```

**Parameters:**

* `float` **t**: interpolation ratio.
* `Quat` **q1**: the quaternion used in lerp operation.
* `Quat` **q2**: the quaternion used in lerp operation.
  
**Returns `Quat`:**

the lerp quaternion result.

----
### `static` slerp

This method will spherically interpolate between quaternions a and b by ratio t.

```cpp
Quat slerp(float t, Quat q1, Quat q2)
```

**Parameters:**

* `float` **t**: interpolation ratio.
* `Quat` **q1**: the quaternion used in slerp operation.
* `Quat` **q2**: the  quaternion used in slerp operation.
  
**Returns `Quat`:**

the slerp quaternion result.

----
### `static` squad

This method will perform spherical cubic interpolation between quaternions by ratio t.

```cpp
Quat squad(float t, Quat q1, Quat q2, Quat q3)
```

**Parameters:**

* `float` **t**: interpolation ratio.
* `Quat` **q1**: the quaternion used in squad operation.
* `Quat` **q2**: the quaternion used in squad operation.
* `Quat` **q3**: the quaternion used in squad operation.
  
**Returns `Quat`:**

the squad quaternion result.

----
### `static` lookRotation
!!! info "Available since version: 0.2.1"

This method will create a rotation based off specified forward and up vectors.

```cpp
Quat lookRotation(Vec3 forward, Quat up)
```

**Parameters:**

* `Vec3` **forward**: the forward direction which object is looking at.
* `Quat` **up**: the up direction.
  
**Returns `Quat`:**

the rotation in form of quaternion.

----
