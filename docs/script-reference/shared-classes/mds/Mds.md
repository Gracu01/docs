---
title: 'Mds'
---
# `static class` Mds <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.0"

This class represents mds manager for conversion between mds id & mds instance.
This manager will work for every registered mds in `mds.xml` file.


## Properties
No properties.

----

## Methods
### id

This method will convert the mds name to mds id.

```cpp
int id(string mdsName)
```

**Parameters:**

* `string` **mdsName**: the mds name, e.g: `"HumanS_Sprint.mds"`.
  
**Returns `int`:**

the unique mds id.

----
### name

This method will convert the mds id to mds name.

```cpp
string name(int mdsId)
```

**Parameters:**

* `int` **mdsId**: the mds id.
  
**Returns `string`:**

the mds name, e.g: `"HumanS_Sprint.mds"`

----
