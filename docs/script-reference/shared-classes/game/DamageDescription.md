---
title: 'DamageDescription'
---
# `class` DamageDescription <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2.1"

This class represents damage information.


## Properties
### `int` flags 

Represents the damage flags.

----
### `int` damage 

Represents the total damage taken.

----
### `int` item_id 
!!! note
    Can be `-1` if there is no weapon.

Represents the weapon used to deal damage.

----
### `int` distance 

Represents the total distance, calculated from origin point to target.

----

## Methods
No methods.

----
