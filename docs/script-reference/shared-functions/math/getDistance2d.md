---
title: 'getDistance2d'
---
# `function` getDistance2d <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the 2d distance between two points.

## Declaration
```cpp
float getDistance2d(float x1, float y1, float x2, float y2)
```

## Parameters
* `float` **x1**: the position on X axis of the first point.
* `float` **y1**: the position on Y axis of the first point.
* `float` **x2**: the position on X axis of the second point.
* `float` **y2**: the position on Y axis of the second point.
  
## Returns `float`
Returns the calculated 2d distance between two points as floating point number.

