---
title: 'getStackTop'
---
# `function` getStackTop <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.0"

This function will get the current stack elements count on squirrel stack.  
It is a equivalent of `sq_gettop` from Squirrel API.

## Declaration
```cpp
int getStackTop()
```

## Parameters
No parameters.
  
## Returns `int`
The current elements count on squirrel stack.

