---
title: 'blob.readstring'
---
# `function` blob.readstring <font size="4">(shared-side)</font>
!!! info "Available since version: 0.2"

This method will read data from a blob into a new string.

## Declaration
```cpp
string blob.readstring(int numberOfBytes)
```

## Parameters
* `int` **numberOfBytes**: the number of bytes to read
  
## Returns `string`
Returns the blob-sourced bytes as a string.

