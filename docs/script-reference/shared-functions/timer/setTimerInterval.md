---
title: 'setTimerInterval'
---
# `function` setTimerInterval <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the timer interval.

## Declaration
```cpp
void setTimerInterval(int timerId, int interval)
```

## Parameters
* `int` **timerId**: the timer id.
* `int` **interval**: the interval time in milliseconds. The smallest acceptable interval is `50`.
  

