---
title: 'setTimer'
---
# `function` setTimer <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will create the timer which will be excecuted X times between the time interval.

## Declaration
```cpp
int setTimer(function func, int interval, int excecuteTimes, ... args)
```

## Parameters
* `function` **func**: the function reference which will be executed between the time interval.
* `int` **interval**: the interval time in milliseconds. The smallest acceptable interval is `50`.
* `int` **excecuteTimes**: the number which corresponds how many times the function will be called. If you pass `0`, the timer will run infinitely.
* `...` **args**: the additional optional parameters, that will be passed to each function call.
  
## Returns `int`
Returns the timer id.

