---
title: 'getTimerExecuteTimes'
---
# `function` getTimerExecuteTimes <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will get the timer excecute times.

## Declaration
```cpp
int getTimerExecuteTimes(int timerId)
```

## Parameters
* `int` **timerId**: the timer id.
  
## Returns `int`
Returns the timer excecute times.

