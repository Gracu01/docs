---
title: 'setTimerExecuteTimes'
---
# `function` setTimerExecuteTimes <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will set the timer excecute times.

## Declaration
```cpp
void setTimerExecuteTimes(int timerId, int excecuteTimes)
```

## Parameters
* `int` **timerId**: the timer id.
* `int` **excecuteTimes**: the number which corresponds how many times the function will be called. If you pass `0`, the timer will run infinitely.
  

