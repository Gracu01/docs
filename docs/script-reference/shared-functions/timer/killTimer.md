---
title: 'killTimer'
---
# `function` killTimer <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will stop the timer with specified id.

## Declaration
```cpp
void killTimer(int timerId)
```

## Parameters
* `int` **timerId**: the timer id.
  

