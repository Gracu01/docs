---
title: 'callEvent'
---
# `function` callEvent <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will notify (call) every handler bound to specified event.

## Declaration
```cpp
bool callEvent(string eventName, ... arguments)
```

## Parameters
* `string` **eventName**: the name of the event.
* `...` **arguments**: the variable number of arguments that should match event arguments.
  
## Returns `bool`
`true` when event wasn't cancelled, otherwise `false`.

