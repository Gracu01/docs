---
title: 'addEvent'
---
# `function` addEvent <font size="4">(shared-side)</font>
!!! info "Available since version: 0.0.1"

This function will register a new event with specified name.  
Events can be used to notify function(s) when something will happen, like player joins the server, etc.

## Declaration
```cpp
bool addEvent(string eventName)
```

## Parameters
* `string` **eventName**: the name of the event.
  
## Returns `bool`
`true` when event was successfully registered, `false` if event with specified name already exists.

