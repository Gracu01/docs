---
title: 'getPlayersCount'
---
# `function` getPlayersCount <font size="4">(shared-side)</font>
!!! info "Available since version: 0.1.10"

This function will return the number of online players on the server.

## Declaration
```cpp
int getPlayersCount()
```

## Parameters
No parameters.
  
## Returns `int`
Number of players on the server.

