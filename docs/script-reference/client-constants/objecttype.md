---
title: 'ObjectType'
---
# `constants` ObjectType <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `OBJ_BASENPC` | Represents Base Npc type (unused by the squirrel scripts). |
| `OBJ_PED` | Represents pedestrian npc type (unused by the squirrel scripts). |
| `OBJ_PLAYER` | Represents player npc type. |
| `OBJ_LOCALPLAYER` | Represents local player (hero) npc type. |
| `OBJ_NPC` | Represents npc type (unused by the squirrel scripts). |
| `OBJ_LOCALNPC` | Represents local npc type. |
| `OBJ_ITEM` | Represents item type (unused by the squirrel scripts). |
