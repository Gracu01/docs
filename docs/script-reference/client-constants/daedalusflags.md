---
title: 'DaedalusFlags'
---
# `constants` DaedalusFlags <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DAEDALUS_FLAG_CONST` | Represents const flag in daedalus scripting language (e.g: constant variables have this flag). |
| `DAEDALUS_FLAG_RETURN` | Represents return flag in daedalus scripting language (e.g: functions that returns have this flag). |
| `DAEDALUS_FLAG_CLASSVAR` | Represents class variable flag in daedalus scripting language. |
| `DAEDALUS_FLAG_EXTERNAL` | Represents external flag in daedalus scripting language (e.g: functions bind in cpp to daedalus have this flag). |
| `DAEDALUS_FLAG_MERGED` | Represents merged flag in daedalus scripting language (e.g: instances that was overriden by instance with the same name have this flag). |
