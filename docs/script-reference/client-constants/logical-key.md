---
title: 'Logical key'
---
# `constants` Logical key <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `GAME_LEFT` | Settings key that turns player to left. |
| `GAME_RIGHT` | Settings key that turns player to right. |
| `GAME_UP` | Settings key that moves player forward. |
| `GAME_DOWN` | Settings key that moves player backward. |
| `GAME_ACTION` | Settings key that's responsible for interaction (e.g. CTRL, LMB). |
| `GAME_ACTION2` | Settings key that's responsible for interaction (i.e. CTRL, LMB), and is unused by the game. |
| `GAME_SLOW` | Settings key that changes player movement to walking. |
| `GAME_WEAPON` | Settings key that draws player weapon. |
| `GAME_SMOVE` | Settings key that makes player jump. |
| `GAME_SMOVE2` | Settings key that is unused. |
| `GAME_SHIFT` | Settings key that is unused. |
| `GAME_END` | Settings key that's responsible for opening main menu screen and closing various menus. |
| `GAME_INVENTORY` | Settings key that's responsible for opening inventory menu. |
| `GAME_LOOK` | Settings key that makes player move head in camera direction. |
| `GAME_SNEAK` | Settings key that changes player movement to sneaking. |
| `GAME_STRAFELEFT` | Settings key that moves player left. |
| `GAME_STRAFERIGHT` | Settings key that moves player right. |
| `GAME_SCREEN_STATUS` | Settings key that opens status screen menu. |
| `GAME_SCREEN_LOG` | Settings key that opens quest log menu. |
| `GAME_SCREEN_MAP` | Settings key that opens map menu. |
| `GAME_LOOK_FP` | Settings key that's responsible for changing perspective to first-person. |
| `GAME_LOCK_TARGET` | Settings key that locks target. Used only for debugging. |
| `GAME_PARADE` | Settings key that makes player parry or dodge an attack while in fight mode. |
| `GAME_ACTIONLEFT` | Settings key that makes player perform left attack. |
| `GAME_ACTIONRIGHT` | Settings key that makes player perform right attack. |
| `GAME_LAME_POTION` | Settings key that makes player drink mana potion. |
| `GAME_LAME_HEAL` | Settings key that makes player drink health potion. |
