---
title: 'MobInterDirection'
---
# `constants` MobInterDirection <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `MOBINTER_DIRECTION_NONE` | Represents none target state direction during mob interation. |
| `MOBINTER_DIRECTION_UP` | Represents up target state direction during mob interaction. |
| `MOBINTER_DIRECTION_DOWN` | Represents down target state during mob interaction. |
