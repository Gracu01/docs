---
title: 'Mouse'
---
# `constants` Mouse <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `MOUSE_WHEELUP` | Represents mouse wheel up movement |
| `MOUSE_WHEELDOWN` | Represents mouse wheel up movement |
| `MOUSE_BUTTONLEFT` | Represents left mouse button |
| `MOUSE_BUTTONRIGHT` | Represents right mouse button |
| `MOUSE_BUTTONMID` | Represents middle mouse button |
| `MOUSE_XBUTTON1` | Represents first extended mouse button |
| `MOUSE_XBUTTON2` | Represents second extended mouse button |
| `MOUSE_XBUTTON3` | Represents third extended mouse button |
| `MOUSE_XBUTTON4` | Represents fourth extended mouse button |
| `MOUSE_XBUTTON5` | Represents fifth extended mouse button |
