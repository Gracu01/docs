---
title: 'BodyState'
---
# `constants` BodyState <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `BS_STAND` | Represents player bodystate while standing. |
| `BS_WALK` | Represents player bodystate while walking. |
| `BS_SNEAK` | Represents player bodystate while sneaking. |
| `BS_RUN` | Represents player bodystate while running. |
| `BS_SPRINT` | Represents player bodystate while sprinting. |
| `BS_SWIM` | Represents player bodystate while swimming. |
| `BS_CRAWL` | Represents player bodystate while crawling. |
| `BS_DIVE` | Represents player bodystate while diving. |
| `BS_JUMP` | Represents player bodystate while jumping. |
| `BS_CLIMB` | Represents player bodystate while climbing. |
| `BS_FALL` | Represents player bodystate while falling. |
| `BS_SIT` | Represents player bodystate while sitting. |
| `BS_LIE` | Represents player bodystate while lying on the floor. |
| `BS_INVENTORY` | Represents player bodystate while taking actions in inventory (e.g. dropping item). |
| `BS_ITEMINTERACT` | Represents player bodystate while using item. |
| `BS_MOBINTERACT` | Represents player bodystate while interacting with mob. |
| `BS_MOBINTERACT_INTERRUPT` | Represents player bodystate while interaction with mob is interrupted. |
| `BS_TAKEITEM` | Represents player bodystate while picking up item. |
| `BS_DROPITEM` | Represents player bodystate while dropping item. |
| `BS_THROWITEM` | Represents player bodystate while throwing item. |
| `BS_PICKPOCKET` | Represents player bodystate while pickpocketing. |
| `BS_STUMBLE` | Represents player bodystate while stumbling (UNUSED). |
| `BS_UNCONSCIOUS` | Represents player bodystate while being uncoscious. |
| `BS_DEAD` | Represents player bodystate while dead. |
| `BS_AIMNEAR` | Represents player bodystate while aiming at nearby target. |
| `BS_AIMFAR` | Represents player bodystate while aiming at far target. |
| `BS_HIT` | Represents player bodystate while being hit. |
| `BS_PARADE` | Represents player bodystate while blocking incoming attack. |
| `BS_CASTING` | Represents player bodystate while spell casting. |
| `BS_PETRIFIED` | Represents player bodystate while being petrified. |
| `BS_CONTROLLING` | Represents player bodystate while controlling target (UNUSED). |
| `BS_MAX` | Represents max count of bodystates. |
