---
title: 'Console'
---
# `constants` Console <font size="4">(client-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `CONSOLE_COMMAND_NOT_FOUND` | Represents command not found status in [onConsole](../../client-events/input/onConsole/) event event. |
| `CONSOLE_COMMAND_FOUND` | Represents command found status in [onConsole](../../client-events/input/onConsole/) event. |
