---
title: 'General'
---
# `constants` General <font size="4">(shared-side)</font>

| Name         | Description                          |
| :----------: | :----------------------------------- |
| `DEBUG_MODE` | Represents debug mode state. |
| `CLIENT_SIDE` | `true` if parsed script is on client-side, otherwise `false`. |
| `SERVER_SIDE` | `true` if parsed script is on server-side, otherwise `false`. |
