---
title: 'ItemsGround'
---
# `static class` ItemsGround <font size="4">(server-side)</font>
!!! info "Available since version: 0.1.0"

This class represents item ground manager.


## Properties
No properties.

----

## Methods
### id

This method will retrieve the item ground object by its unique id.

```cpp
ItemGround id(int itemGroundId)
```

**Parameters:**

* `int` **itemGroundId**: the unique item ground id.
  
**Returns `ItemGround`:**

the item ground object or `null` if the object cannot be found.

----
### spawn

This method will create the item ground.

```cpp
ItemGround spawn(int itemId, int itemAmount, float x, float y, float z, string world, int virtualWorld = 0)
```

**Parameters:**

* `int` **itemId**: the item id.
* `int` **itemAmount**: the item amount.
* `float` **x**: the x position on the world.
* `float` **y**: the y position on the world.
* `float` **z**: the z position on the world.
* `string` **world**: the world path.
* `int` **virtualWorld**: the virtual world id.
  
**Returns `ItemGround`:**

the item ground object.

----
### destroy

This method will destroy the item ground by it's unique id.

```cpp
void destroy(int itemGroundId)
```

**Parameters:**

* `int` **itemGroundId**: the item ground unique id.
  

----
