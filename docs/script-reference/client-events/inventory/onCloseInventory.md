---
title: 'onCloseInventory'
---
# `event` onCloseInventory <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered when user is closing inventory.

## Parameters
No parameters.

