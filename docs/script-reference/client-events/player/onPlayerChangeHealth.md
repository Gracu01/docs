---
title: 'onPlayerChangeHealth'
---
# `event` onPlayerChangeHealth <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"

This event is triggered when player health changes.

## Parameters
```c++
int playerid, int oldHP, int newHP
```

* `int` **playerid**: the id of the player whose health points gets changed.
* `int` **oldHP**: the previous health points of the player.
* `int` **newHP**: the new health points of the player.

