---
title: 'onPlayerCreate'
---
# `event` onPlayerCreate <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered when new player joins to the server.

## Parameters
```c++
int id
```

* `int` **id**: the id of connected player.

