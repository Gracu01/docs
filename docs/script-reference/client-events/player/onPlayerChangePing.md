---
title: 'onPlayerChangePing'
---
# `event` onPlayerChangePing <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This event is triggered when server updates player ping.

## Parameters
```c++
int id, int ping
```

* `int` **id**: the id of player which ping is updated.
* `int` **ping**: the player ping.

