---
title: 'onPlayerParade'
---
# `event` onPlayerParade <font size="4">(client-side)</font>
!!! info "Available since version: 0.2.1"
!!! tip "This event can be canceled"
!!! note
    Currently this event is triggered only when attacks from hero are blocked.

This event is triggered when some player or npc, will successfully block attack.

## Parameters
```c++
int playerid, int attackerid
```

* `int` **playerid**: the id of the defending player.
* `int` **attackerid**: the id of the attacking player.

