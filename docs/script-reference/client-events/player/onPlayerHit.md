---
title: 'onPlayerHit'
---
# `event` onPlayerHit <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.2"
!!! tip "This event can be canceled"
!!! note
    Item id in damage description is available only when hero is killer!
!!! note
    In case when other connected player damages any other player or npc, this event cannot be cancelled!
!!! note
    In case when user damages any other connected player and this event is cancelled, the server will not get notified!

This event is triggered when a player or npc is damaged.

## Parameters
```c++
int killerid, int playerid, DamageDescription description
```

* `int` **killerid**: the id of the killer. If killerid is set to `-1`, it means that there was no killer. In this particular case damage source can be fall from a tall object or scripts.
* `int` **playerid**: the id of the player who was damaged.
* `DamageDescription` **description**: a structure containing damage information. For more information see [DamageDescription](../../../shared-classes/game/DamageDescription/)

