---
title: 'onPlayerUseItem'
---
# `event` onPlayerUseItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.2"

This event is triggered when hero/npc uses, interacts, opens or consumes any item.

## Parameters
```c++
int id, int itemId, int from, int to
```

* `int` **id**: the id of the player who uses item.
* `int` **itemId**: the id of an item.
* `int` **from**: represents previous state for interact item.
* `int` **to**: represents current state for interact item.

