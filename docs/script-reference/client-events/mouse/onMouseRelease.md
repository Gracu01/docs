---
title: 'onMouseRelease'
---
# `event` onMouseRelease <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.2"

This event triggers whenever the user releases any button on his mouse.

## Parameters
```c++
int button
```

* `int` **button**: this refers to the button clicked on the mouse. For more information see [mouse buttons](../../../client-constants/mouse/).

