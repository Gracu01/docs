---
title: 'onSink'
---
# `event` onSink <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"

This event is triggered every time when hero is damage caused by drowning.
You can change value of damage, by using `eventValue` function.

## Parameters
No parameters.

