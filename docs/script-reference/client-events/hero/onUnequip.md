---
title: 'onUnequip'
---
# `event` onUnequip <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered when hero is unequipping item.

## Parameters
```c++
string instance
```

* `string` **instance**: an instance name of the item.

