---
title: 'onDropItem'
---
# `event` onDropItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! tip "This event can be canceled"

This event is triggered when hero drops an item from his inventory to the ground.

## Parameters
```c++
string instance, int amount
```

* `string` **instance**: an instance name of the dropped item.
* `int` **amount**: an amount of the dropped item.

