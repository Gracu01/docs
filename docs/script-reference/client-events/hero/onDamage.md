---
title: 'onDamage'
---
# `event` onDamage <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.1"
!!! tip "This event can be canceled"

This event is triggered when hero is damaged, by not player or npc.

## Parameters
```c++
DamageDescription description
```

* `DamageDescription` **description**: a structure containing damage information. For more information see [DamageDescription](../../../shared-classes/game/DamageDescription/)

