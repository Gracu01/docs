---
title: 'onTakeFocus'
---
# `event` onTakeFocus <font size="4">(client-side)</font>
!!! info "Available since version: 0.1.0"
!!! note
    In case when user changed focus to a new target, firstly event `onLostFocus` is called.

This event is triggered when hero focuses object like player, npc or vob.

## Parameters
```c++
int type, int id, string name
```

* `int` **type**: the type of currently focused vob. For more information see [Vob types](../../../client-constants/vob/).
* `int` **id**: the id of currently focused vob. If focused target is not player or npc this value is `-1`.
* `string` **name**: focused target displayed name.

