---
title: 'onTakeItem'
---
# `event` onTakeItem <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"
!!! tip "This event can be canceled"
!!! note
    Even if this event is triggered it doesn't mean, that hero will get item to his inventory. It only means, that the notification will be sent to the server. Server is the last to decide if this item will belong to hero or not. Canceling this event will prevent client from notifying server about this occurrence.

This event is triggered when hero takes an item from the ground to his inventory.

## Parameters
```c++
string instance, int amount
```

* `string` **instance**: an instance name of the dropped item.
* `int` **amount**: an amount of the dropped item.

