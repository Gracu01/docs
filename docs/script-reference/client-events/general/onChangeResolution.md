---
title: 'onChangeResolution'
---
# `event` onChangeResolution <font size="4">(client-side)</font>
!!! info "Available since version: 0.0.4"

This event is triggered when resolution of the game has been changed.

## Parameters
No parameters.

