### Download

The newest server files can be downloaded from [our website](https://gothic-online.com.pl).

### Server

You can run Gothic 2 Online server on multiple platforms.  

#### Contents

The default server package contains some of the template scripts such as:  

##### Default scripts
- chat  
- network statistics  
- player list  
- Admin Control Panel  

##### Example scripts
The package also contains some of the usage examples of certain parts of Gothic 2 Online scripting API.
There can be found under `server/examples` subfolder.

##### Configuration files
- `config.xml`  
    This file contains default server configuration, [read more here](../configuration).
- `instances.xml`  
    This file contains every npc and item instance information from `Gothic.dat` file.
- `mds.xml`  
    This file contains every registered game mds overlay that can be manipulated via [applyPlayerOverlay](../../script-reference/server-functions/player/applyPlayerOverlay/) and [removePlayerOverlay](../../script-reference/server-side-functions/player/removePlayerOverlay/) functions.  
    Also, by using [Mds.id](../../script-reference/shared-classes/mds/Mds/#id) and [Mds.name](../../script-reference/shared-classes/mds/Mds/#name) you're able to convert the *mds id* to *mds instance* and vice versa.  
    Each time when you start the server, the *mds ids* will be randomly assigned.
- `bans.xml`  
    This file contains every banned player, and the metadata assigned with him (nick, ip, MAC, serial).

#### Running on Windows

##### running task
In order to run Gothic 2 Online server under Windows we need to:

- extract the downloaded server package
- execute the `G2O_Server.exe`

##### formatting windows-12xx output
In order to properly display localized messages inside the console, you need to set the proper encoding page in windows console before running the g2o server.  
On windows there's a utility command called [chcp](https://learn.microsoft.com/pl-pl/windows-server/administration/windows-commands/chcp) that sets the proper  encoding page as a output for the windows console to properly display the localized messages:

- `chcp 1250 | G2O_Server.x86.exe`

#### Running on Linux

##### regular task
In order to run Gothic 2 Online server under Linux we need to:

- extract the downloaded server package
- run command `chmod +x G2O_Server.*` to add execute rights for the file.  
- run command `./G2O_Server.*`

##### background task
In order to run Gothic 2 Online server under Linux as a background task we need to:

- extract the downloaded server package
- run command `chmod +x G2O_Server.*` to add execute rights for the file.  
- run command `./G2O_Server.* &`
- press `CTRL+C` to detach from the running process

To kill the Gothic 2 Online server running in the background:

- run command ``kill -SIGKILL `pidof G2O_Server.*` ``

##### formatting windows-12xx output
In order to properly display localized messages inside the terminal, you need to convert the text from `windows-12xx` encoding page to `utf-8`
because linux terminal supports natively `utf-8` encoding. On linux there's a utility called [inconv](https://man7.org/linux/man-pages/man1/iconv.1.html) that is used to convert the text encoding
from one format to another, you can pipeline the console output via this utility to properly display the localized messages:

-  `iconv -f windows-1250 -t utf-8 | ./G2O_Server.*`